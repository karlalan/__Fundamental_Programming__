<?php

$numbers = array( 1, 2, 3, 4, 5 );
$strings = array( "A", "B", "C", "D", "E" );

////////////////////
// For loop
////////////////////
for( $i = 0; $i < count( $numbers ); ++$i ){
  print_r( $numbers[$i] . ", " );
}

print_r( "\n" );

////////////////////
// while loop
////////////////////
$index = 0;
while( $index < count( $strings ) ){
  print_r( $strings[$index] . ", " );
  $index++;
}

print_r( "\n" );

$index2 = 0;
do{
  print_r( $strings[$index2] . ", " );
  $index2++;
}while( $index2 < count( $strings ) );

print_r( "\n" );

////////////////////
// foreach loop
////////////////////
foreach ( $numbers as $index => $num) {
  print_r( $num . "'s key is: " . $index ."\n" );
}


print(phpversion());
