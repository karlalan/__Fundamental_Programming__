# # for using sys.stdout.write
# import sys

# Numbers = [ 1, 2, 3, 4, 5 ]
# Strings = [ "A", "B", "C", "D", "E" ]

# #################
# # For loop
# #################
# for num in Numbers:
#   print( num, end = ", " )

# print( "" )

# for s in Strings:
#   ## it only can print data with string type
#   sys.stdout.write( s + ", " )

# print( "" )

# ## list can be created by for loop
# string = "abcdefghijklmn"
# condition = "def"
# list1 = [ x for x in string if x not in condition ]

# print( list1 )

# #################
# # While loop
# #################
# index = 0
# while index < len( Numbers ):
#   print( Numbers[index], end = " " )
#   index += 1

# print( "" )

