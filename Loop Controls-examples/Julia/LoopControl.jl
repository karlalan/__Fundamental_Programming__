α = [ x for x in 0:6 ]
println( α )

if α[1] > 1
  println( "greater than 1" )
elseif α[1] == 1
  println( "equal to one" )
else
  println( "out of condition" )
end


ι = 1
while ι <= 3
  println(ι)
  ι += 1
end


## "string" => with double quotation
## 'char' => with single quotaiton
β = [ c => i for ( c, i ) in zip( 'a':'f', 1:6 ) ]
println( β )


for i = 1:6
  println(i)
end

### ∈ \in
###
for s ∈ ["aa","bb"]
  println(s)
end

for i in [1,2,7]
  println(i)
end