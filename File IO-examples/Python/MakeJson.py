import json
from sys import stdin, stdout
from datetime import datetime, timedelta
import copy

example = {
  "SIIRESAKI_JUSHO_NO": "1022",
  "HINMOKU_NO": "BB74021",
  "ZAIKO_SURYO": 180,
  "SAKUSEI_DATE": "",
}
y = 2017
m = 1
d = 16

Dict = []
DictPush = Dict.append
for k in range(180):
  date = (datetime(y,m,d)+timedelta(days=-k)).strftime("%Y%m%d")
  temp = copy.deepcopy(example)
  temp["SAKUSEI_DATE"] = date
  for j in range(1984):
    DictPush(temp)

with open( 'SAP_BUKABETUZAIKO_RUISEKITestData.json', 'w', encoding='utf-8' ) as f:
  json.dump( Dict, f, ensure_ascii=False, indent=2, separators=(',', ': ') )
stdout.write( "Complete!!\n" )