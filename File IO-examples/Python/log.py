import logging as lg

mesFormat='%(asctime)s %(message)s'
dateFormat = '%Y-%d-%m %I:%M:%S'
fileName = 'error.log'

lg.basicConfig(
   format=mesFormat
  ,datefmt = dateFormat
  ,filename = fileName
  ,level=lg.DEBUG
)

lg.warning('apple test')