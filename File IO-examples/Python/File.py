from sys import stdin, stdout, stderr

filepath = 'test.txt'

## open a file with read mode
res = ''
try:
  f = open(filepath,'r+')
  res = f.read()
  f.close()
except Exception:
  err = 'File {} cannot be open.'.format(filepath)
  stdout.writelines('%s\n' % err)
else:
  stdout.writelines('File is closed: {}\n'.format(f.closed))


# stdout.writelines('Result is %s\n' % res)
