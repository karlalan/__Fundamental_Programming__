import openpyxl as opxl
import json
from sys import stdin, stdout

def GetValueList( data ):
  return [[cell.value if cell.value else "" for cell in row] for row in data]

def MakeValueDict( data):
  Dict = []
  DictPush = Dict.append
  Keys = data[0]
  ColLen = len(Keys)
  for item in data[1:]:
    tempDict = {Keys[i]: item[i] for i in range(ColLen)}
    DictPush(tempDict)
  return Dict

def PickUpFileName( filepath ):
  return filepath.split(".")

def TranCsvToJson( filepath, filename ):
  csv = open(filepath, 'r')
  data = [line.replace("\n","").replace("\"","").split(",") for line in csv.readlines()]
  DataDict = MakeValueDict(data)

  if len(DataDict):
    with open( filename + '.json', 'w', encoding='utf-8' ) as f:
      json.dump( DataDict, f, ensure_ascii=False, indent=2, separators=(',', ': ') )
    stdout.write( "csvからjsonへの変換成功です\n" )
  else:
    stdout.write( "No Data\n" )

def TranXlsxToJson( filepath, filename ):
  wb = opxl.load_workbook( filepath )
  SavedData = GetValueList( wb['Sheet1'] )
  DataDict = MakeValueDict(SavedData)

  if len(DataDict):
    with open( filename + '.json', 'w', encoding='utf-8' ) as f:
      json.dump( DataDict, f, ensure_ascii=False, indent=2, separators=(',', ': ') )
    stdout.write( "xlsxからjsonへの変換成功です\n" )
  else:
    stdout.write( "No Data\n" )

if __name__ == '__main__':
  filepath = stdin.readline().replace("\n","")
  if filepath:
    try:
      fileInfo = PickUpFileName( filepath )
      filename = fileInfo[0]
      extension = fileInfo[1]
      if extension == "csv":
        TranCsvToJson( filepath, filename )
      else:
        TranXlsxToJson( filepath, filename )
    except Exception as e:
      stdout.write( "Error: ファイルが見つかりません\n%s" % e )