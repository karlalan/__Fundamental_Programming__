<?php
class XmlFile
{
  private $path;
  private $xml;

  function __construct( $arg )
  {
    $this->path = $arg;
  }

  public function LoadXml()
  {
    $string = file_get_contents( $this->path ) or die( "Unable to open" );
    $this->xml = new DOMDocument( '1.0', 'UTF-8' );
    $this->xml->formatOutput = true;
    $this->xml->load( $string );
  }

  public function GetContentOfXml()
  {
    return $this->xml;
  }

  public function SaveXml()
  {
    $this->xml->asXML( $this->path );
  }
}
$filepath = "http://localhost/Yutaka/xml/rsslist.xml";

$xml = new XmlFile( $filepath );
$xml->LoadXml();
print_r($xml->GetContentOfXml());