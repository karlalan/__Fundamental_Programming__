#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char const *argv[])
{
  double dbNum = 200.34;
  char str[] = "dbNum is ";

  /////////////////////////
  /// nested if control
  /////////////////////////
  if( dbNum > 300 ){
    printf( strcat(str, "larger than 300 \n") );
  } else if( dbNum > 200 ){
    printf( "%slarger than 200 \n", str );
  } else {
    printf( strcat( str, "less than 200 \n" ) );
  }

  /////////////////////////
  /// ternary operator
  /////////////////////////
  const char *str2 = ( dbNum > 300 ) ? "larger than 300" : "less than 300";
  printf("str2: %s\n", str2 );

  /////////////////////////
  /// switch control
  /// The expression used in a switch statement must
  /// have an integral or enumerated type, or be of a
  /// class type in which the class has a single conversion
  /// function to an integral or enumerated type.
  /////////////////////////
  switch( (int)dbNum ){
    case 100:
      printf( strcat( str, "100\n" ) );
      break;
    case 200:
      printf( strcat( str, "200\n" ) );
      break;
    case 300:
      printf( strcat( str, "200\n" ) );
      break;
    default:
      printf( strcat( str, "not 100, 200, and 300.\n" ) );
      break;
  }


  exit( EXIT_SUCCESS );
}