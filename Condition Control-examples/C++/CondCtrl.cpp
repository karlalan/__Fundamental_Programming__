#include <iostream>
#include <string>

using namespace std;

int main(int argc, char const *argv[])
{
  int num = 345;
  std::string str = "num is ";

  /////////////////////////
  /// nested if control
  /////////////////////////
  std::string str2;
  if( num > 400 ){
    str2 = "larger than 400.";
  } else if( num > 300 ){
    str2 = "larger than 300.";
  } else {
    str2 = "less than 300.";
  }
  cout << str + str2 << endl;

  /////////////////////////
  /// ternary operator
  /////////////////////////
  const char *str4 = ( num > 300 ) ? "larger than 300" : "less than 300";
  cout << str4 << endl;

  /////////////////////////
  /// switch control
  /// The expression used in a switch statement must
  /// have an integral or enumerated type, or be of a
  /// class type in which the class has a single conversion
  /// function to an integral or enumerated type.
  /////////////////////////
  std::string str3;
  switch( num ){
    case 342:
      str3 = "342";
      break;
    case 345:
      str3 = "345";
      break;
    default:
      str3 = "out of conditions.";
      break;
  }
  cout << str + str3 << endl;

  /////////////
  //
  //
  /////////////
  const char *s = "Hello World";
  while(*s){
    cout << *s++;
  }


  return 0;
}