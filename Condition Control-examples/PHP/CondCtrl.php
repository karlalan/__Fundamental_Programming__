<?php
$num = 200.34;
$res = "num is ";

/////////////////////////
/// nested if control
/////////////////////////
if( $num > 300 ){
  $res .= "larger than 300";
} else {
  if( $num > 200 ){
    $res .= "larger than 200";
  } else {
    $res .= "less than 200";
  }
}

var_dump( $res );

/////////////////////////
/// ternary operator
/////////////////////////
$res3 = ( $num > 300 ) ? "larger than 300" : "less than 300";
var_dump( $res3 );

/////////////////////////
/// switch control
/// the statement can be any type of data
/////////////////////////
$res2 = "num is ";
switch( $num ){
  case 300:
    $res2 .= "300.";
    break;
  case 200:
    $res2 .= "200.";
    break;
  case 200.34:
    $res2 .= "200.34";
    break;
  default:
    $res2 .= "out of conditions.";
    break;
}

var_dump( $res2 );

