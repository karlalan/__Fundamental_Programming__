#### partial function
from functools import partial
def test( x,y,z,Q ):
  return (x+y+z)*Q
x,y,z = 3,4,5
g = partial( test,x,y,z )
## The default parameter of g will be the last
## parameter of test
print( g(10) )
## By assigning values to specific parameters in order way,
## the default parameter of h can be changed
h = partial( test, y=3, z=4, Q=5 )
print( h( 12 ) )

################################################################
# Use * to unpack the list so all elements of it can be passed as
# different parameters
def test2( a, b, c, d ):
  print( d, a, b, c )

List = [1,2,3,4]
test2( *List )

def t( *args ):
  print( args )
t( *[1,2,3,4 ])

# When we do not know how many parameters will be passed to a
# function, we can use Packing to pack all elements in a tuple
def test3( *args ):
  print( ",".join( map( str, args ) ) )

test3( 1,2,3,4,5,6 )

# ** is used for unpacking dictionary.
def test4( a,b,c ):
  print( a, b, c )

dictionary = { 'a':1, 'b':2, 'c':3 }
test4( **dictionary )

def test5( **kwargs ):
  for key in kwargs:
    print( "%s == %d" % ( key, kwargs[key] ) )
test5( **dictionary )

################################################################
## print string converting to int base 2
string = "100101"
print( int( string, 2 ) )

## convert a character to int based on ASCII
print( ord( 'a' ) )

## convert a tuple of order ( key, value ) to a dictionary
tup = ( ('a',1), ('b',10), ('c',100) )
print( dict( tup ) )

## convert real number to complex number
print( complex( 1, 4 ) )


################################################################
## Encoding

abefore = "apple"
testa = b"apple"
aAfter = abefore.encode( "ASCII" )
print( aAfter == testa )


################################################################
## Get method for dictionary
mydict = { 'a':123, 'b':456 }
print( mydict.get( 'a' ) )
defaultMessage = 'Not found!'
print( mydict.get( 'c', defaultMessage ) )


################################################################
# Handle missing key in dictionaries
# Method 1: use get method ( as above )
# Method 2: use setdefault
# Method 3: use defaultdict

### Method 2
mydict2 = { 'a':1234, 'b':9876 }
mydict2.setdefault( 'c', "Not found" )
print( mydict2.get( 'c' ) )

### Method 3
import collections
mydict3 = collections.defaultdict( lambda: "Not FOUND!!!" )
mydict3.update( mydict2 )
print( mydict3['d'] )

################################################################
#### ChainMap ( module in collections )
partdict1 = { 'alan': 555555 ,  'ban': 23555 }
partdict2 = { 'banana': 255325 , 'cana': 288325 }
partdict3 = { 'sa':999 }

chain = collections.ChainMap( partdict1, partdict2 )
## ---> list
print( chain.maps )
## add a new child
chain = chain.new_child( partdict3 )
## collect all keys
print( list( chain.keys() ) )
## collect all values
print( list( chain.values() ) )

chain.maps = reversed( chain.maps )
print( list( chain.maps ) )


################################################################
# OrderedDict in Python
compareDict = {}
compareDict['A'] = 2
compareDict['B'] = 3
compareDict['C'] = 4
compareDict['D'] = 6
for key, value in compareDict.items():
  print( "%s == %d" % ( key, value ) )
print("")
odDict = collections.OrderedDict()
odDict['A'] = 2
odDict['B'] = 3
odDict['C'] = 4
odDict['D'] = 6
for key, value in odDict.items():
  print( "%s == %d" % ( key, value ) )







