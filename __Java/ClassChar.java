public class ClassChar{

  public static void main(String[] args) {

    char input = 'T';

    Character ch = new Character( input );
    System.out.println( ch.toString() );
    System.out.println( ch.charValue() );

    String s = new String( "TEST" );
    System.out.println( s.toString() );

  }

}