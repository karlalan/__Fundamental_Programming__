public class InstanceOf{
  public static void main(String[] args) {
    String test = "Is test";

    // The operator checks whether the object is of a particular type.
    boolean result = test instanceof String;
    System.out.println( result );
  }
}