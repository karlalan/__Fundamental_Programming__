from sys import stdin, stdout
from time import time

class StringSearch():
    def __init__(self, pattern, arg):
        self.p = pattern
        self.t = arg

    '''
    Simple Method
    '''
    def SimpleWay(self) -> list:
        '''
        i means the position of string.
        j means the position of pattern.
        '''
        i, j, m, n = 0, 0, len(self.p), len(self.t)
        result = []
        while i <= n - m:
            while j < m and self.t[i+j] == self.p[j]:
                j += 1
            if j == m: result.append(i)
            i += 1
            j = 0
        return result

    '''
    Knuth-Morris-Pratt Algorithm
    '''
    def KMPMakeSkipList(self) -> list:
        checked, skip = [], []
        for i, char in enumerate(self.p):
            if char not in checked:
                checked.append(char)
                idx = len(checked) - 1
                skipnum = idx if idx > 0 else idx+1
                skip.append(skipnum)
            else:
                RepeatedFirstIdx = self.p.index(char)
                skipnum = i - RepeatedFirstIdx + 1
                skip.append(skipnum)
        return skip

    def KMPAlgo(self) -> list:
        '''
        i means the position of string.
        j means the position of pattern.
        '''
        i, j, m, n = 0, 0, len(self.p), len(self.t)
        result = []
        skip = self.KMPMakeSkipList()
        while i <= n-m:
            while j < m and self.t[i+j] == self.p[j]:
                j += 1
            if j == m:
                result.append(i)
                j = 0
            i += skip[j]
            j -= skip[j]
            if j == -1: j = 0
        return result

    '''
    Boyer-Moore Algorithm
    '''
    def BMMakeSkipList(self) -> dict:
        skip, pLastIdx = {}, len(self.p)-1
        for rightidx in range(pLastIdx, -1, -1):
            if self.p[rightidx] not in skip:
                skip.update({self.p[rightidx]: pLastIdx-rightidx})
        return skip

    def BMAlgo(self):
        '''
        i means the position of string.
        j means the position of pattern.
        '''
        i, m, n = 0, len(self.p), len(self.t)
        skip, result = self.BMMakeSkipList(), []
        while i <= n-m:
            j = m-1
            while j >= 0 and self.t[i+j] == self.p[j]:
                j -= 1
            if j == -1: result.append(i)
            i += max(skip.get(self.t[i+j], m)+j-m+1, 1)
        return result



if __name__ == '__main__':
    pattern, str1 = 'abcde', 'acccaaaqabbbbaqaaaaa'
    ss = StringSearch(pattern, str1)
    t1 = time()
    SWRes = ss.SimpleWay()
    t2 = time()
    print('Simple Way Result:', SWRes, 'Required time is:', t2-t1)
    t3 = time()
    KMPRes = ss.KMPAlgo()
    t4 = time()
    print('KMP Algorithm Result:', KMPRes, 'Required time is:', t4-t3)
    t5 = time()
    BMRes = ss.BMAlgo()
    t6 = time()
    print('BM Algorithm Result:', BMRes, 'Required time is:', t6-t5)