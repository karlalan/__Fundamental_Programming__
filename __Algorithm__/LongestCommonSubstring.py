from sys import stdin, stdout

class LongCommonSubstring():
    def __init__(self, x, y):
        self.X = x
        self.Y = y

    def Algo(self):
        XLen, YLen = len(self.X), len(self.Y)
        table = [[0 for _y_ in range(YLen+1)] for _x_ in range(XLen+1)]
        LenOfLCS = 0
        for i in range(XLen):
            for j in range(YLen):
                d = 1 if self.X[i] == self.Y[j] else 0
                table[i][j] = max(table[i-1][j-1]+d, table[i-1][j], table[i][j-1])
                if i == XLen-1 and j == YLen-1: LenOfLCS = table[i][j]
        return table



if __name__ == '__main__':
    X = 'abcbdab'
    Y = 'bdcaba'
    LCS = LongCommonSubstring(X, Y)
    t = LCS.Algo()
    print('\n'.join(map(str, t)))