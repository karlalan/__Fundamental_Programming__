## weight array
W = [-1,1,2,4,2,15,3,8,6,7,7]
## value array
V = [-1,5,3,5,3,2,10,3,9,2,2]
## constrain
C = 10

def Knapsack(n, C):
    if C == 0 or n == 0: result = 0
    elif W[n] > C:
        result = Knapsack(n-1, C) ## do not put in knapsack
    else:
        r1 = Knapsack(n-1, C) ## do not put in knapsack
        r2 = V[n] + Knapsack(n-1, C - W[n]) ## put in knapsack
        result = max(r1,r2) ## check value
    return result

print(Knapsack(10,C))


"""
---------------------------
|i\w| 1 | 2 | ... | 10 |
---------------------------
| 1 |
---------------------------
| 2 |
---------------------------
| . |
| . |
| 10|
---------------------------

"""

def DynamicKnapsack(n, C):
    K = [[0 for i in range(C+1)] for j in range(n+1)]
    for i in range(n+1):
        for w in range(C+1):
            if i == 0 or w == 0: K[i][w] = 0
            elif W[i]<=w: K[i][w] = max(V[i]+K[i-1][w-W[i]], K[i-1][w])
            else: K[i][w] = K[i-1][w]
    return K[n][C]

print(DynamicKnapsack(10,C))