from sys import stdin, stdout

class SearchAlgo():
    def __init__(self, graph, start, end):
        self._g_ = graph
        self._s_ = start
        self._e_ = end

    def DFS_Only_ShortPath(self, G, S, goal, path = None):
        if path is None: path = [S]
        if S == goal: yield path
        for nextP in G[S] - set(path):
            yield from self.DFS_Only_ShortPath(G, nextP, goal, path+[nextP])

    def DFS_With_Process(self, G, S, goal, path = None):
        if path is None: path = [S]
        else: path += [S]
        if S == goal: yield path
        for nextP in G[S] - set(path):
            yield from self.DFS_With_Process(G, nextP, goal, path)

    def MainDFS(self):
        '''
        Return the shortest path
        '''
        Res_Short = list(self.DFS_Only_ShortPath(self._g_, self._s_, self._e_))[0]
        '''
        Return path with all visited vertices
        '''
        Res_Process = list(self.DFS_With_Process(self._g_, self._s_, self._e_))[0]
        goalIdx = Res_Process.index(self._e_)
        Res_Process = Res_Process[:goalIdx+1]
        '''
        Print Result
        '''
        Message = "DFS\nThe shortest path is %s.\nThe path with all visited vertices is %s.\n"
        stdout.writelines(Message % (Res_Short, Res_Process))


    def BFS_With_Process(self, G, S, goal):
        queue = [(S, [S])]
        while queue:
            vertex, path = queue.pop(0)
            for nextP in G[vertex] - set(path):
                path += [nextP]
                if nextP == goal: yield path
                else: queue.append((nextP, path))

    def MainBFS(self):
        '''
        Return path with all visited vertices
        '''
        Res_Process = list(self.BFS_With_Process(self._g_, self._s_, self._e_))[0]
        goalIdx = Res_Process.index(self._e_)
        Res_Process = Res_Process[:goalIdx+1]
        '''
        Print Result
        '''
        Message = "BFS\nThe path with all visited vertices is %s.\n"
        stdout.writelines(Message % Res_Process)


if __name__ == '__main__':
    '''
              A
         /    |    \
       B      C      D
      / \    / \    / \
     E   F  G   H  I   J
    '''
    graph = {
        'A': {'B', 'C', 'D'},
        'B': {'A', 'E', 'F'},
        'C': {'A', 'G', 'H'},
        'D': {'A', 'I', 'J'},
        'E': {'B'},
        'F': {'B'},
        'G': {'C'},
        'H': {'C'},
        'I': {'D'},
        'J': {'D'},
    }

    Algo = SearchAlgo(graph, 'A', 'H')
    Algo.MainDFS()
    Algo.MainBFS()