from sys import stdin, stdout

INF = float('inf')

class DijkstraAlgo():
    def __init__(self, nodes, graph, startpoint):
        self._nodes_ = nodes
        self._graph_ = graph
        self._unvisited_ = {node:INF for node in nodes}
        self._visited_ = {}
        self._current_ = startpoint
        self._currentDis_ = 0
        self._unvisited_[self._current_] = self._currentDis_
        self._start_ = startpoint

    def Dijkstra(self):
        while True:
            for neighbour, distance in self._graph_[self._current_].items():
                if neighbour not in self._unvisited_: continue
                newDistance = self._currentDis_+distance
                if self._unvisited_[neighbour] > newDistance:
                    self._unvisited_[neighbour] = newDistance
            self._visited_[self._current_] = self._currentDis_
            del self._unvisited_[self._current_]
            if not self._unvisited_: break
            candidates = [node for node in self._unvisited_.items()]
            self._current_, self._currentDis_ = sorted(candidates, key = lambda x: x[1])[0]

    def Main(self):
        self.Dijkstra()

    def __str__(self):
        self.Main()
        return "The start point is %s, and the result of Dijkstra is: %s" % (self._start_, self._visited_)



if __name__ == '__main__':
    '''
    undirected graph
    '''
    nodes = ('A', 'B', 'C', 'D', 'E', 'F', 'G')
    graph = {
        'B': {'A': 5, 'D': 1, 'G': 2},
        'A': {'B': 5, 'D': 3, 'E': 12, 'F' :5},
        'D': {'B': 1, 'G': 1, 'E': 1, 'A': 3},
        'G': {'B': 2, 'D': 1, 'C': 2},
        'C': {'G': 2, 'E': 1, 'F': 1},
        'E': {'A': 12, 'D': 1, 'C': 1, 'F': 2},
        'F': {'A': 5, 'E': 2, 'C': 16}
    }
    '''
    directed graph
    '''
    dnodes = ('s','a','b','c')
    dgraph = {
        's': {'a': 2, 'b': 6},
        'a': {'b': 3, 's': INF},
        'b': {'c': 2, 's': INF},
        'c': {'b': INF}
    }

    D1 = DijkstraAlgo(dnodes,dgraph,"c")
    print(D1)