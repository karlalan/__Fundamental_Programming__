from sys import stdin, stdout

def Swap(List, idx1, idx2):
    List[idx1], List[idx2] = List[idx2], List[idx1]

def BubbleSort(List):
    L = len(List)
    for i in range(L):
        for j in range(i+1, L):
            if List[i] > List[j]:
                Swap(List, i, j)
    return List

if __name__ == '__main__':
    target = [4, 5, 1, 3, 7, 1, 10, 2, 3]
    SortedRes = BubbleSort(target)
    stdout.writelines("Result of BubbleSort is: %s\n" % SortedRes)