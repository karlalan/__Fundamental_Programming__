from sys import stdin, stdout

test = '#'+stdin.readline().replace('\n','')

def Calc(string):
    s, t, c = 0, 0, 0
    idx = 1
    while idx < len(string):
        char = string[idx]
        if char == 'S':
            s += 1; idx += 1
        elif char == 's':
            s -= 1; idx += 1
        elif char == 'T':
            t += 1; idx += 1
        elif char == 't':
            t -= 1; idx += 1
        elif char == 'j':
            idx = int(string[idx+1:idx+5])
        elif char == 'b':
            idx = int(string[idx+1:idx+5]) if s > 0 else idx+5
        elif char == 'f':
            break
        elif char == 'c':
            string += string[idx+1:idx+5]
            idx += 1
        else:
            idx += 1
    mes = 's is %d. t is %d. c is %d.'
    stdout.writelines(mes % (s,t,c))

Calc(test)