from sys import stdin, stdout

class HeapSort1():
    def __init__(self,arg):
        self._List_ = arg

    def Swap(self, List, idx1, idx2):
        self._List_[idx1], self._List_[idx2] = self._List_[idx2], self._List_[idx1]

    def Heapify(self, List, idx1, idx2):
        l, r = 2*idx1+1, 2*(idx1+1)
        parent = idx1
        if l < idx2 and List[idx1] < List[l]:
            parent = l
        if r < idx2 and List[parent] < List[r]:
            parent = r
        if parent != idx1:
            self.Swap(List, idx1, parent)
            self.Heapify(List, parent, idx2)

    def HeapSort(self, List):
        end = len(List)
        start = end // 2 - 1
        '''
        Transfer array to heap data structure
        '''
        for i in range(start, -1, -1):
            self.Heapify(self._List_, i, end)

        for j in range(end-1, 0, -1):
            self.Swap(self._List_, 0, j)
            self.Heapify(self._List_, 0, j-1)

    def Main(self):
        self.HeapSort(self._List_)
        return self._List_

    def __str__(self):
        return "Result of HeapSort is: %s" % self.Main()


if __name__ == '__main__':
    target = [4, 5, 1, 3, 7, 1, 10, 2, 3]
    H1 = HeapSort1(target)
    print(H1)