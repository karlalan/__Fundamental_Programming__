
def SieveAlgo(n):
    sieve = [True]*(n+1)
    p = 2
    while p*p <= n:
        if sieve[p]:
            for i in range(p*2, n+1, p): sieve[i] = False
        p += 1

    for i in range(2, n+1):
        if sieve[i]: print(i)

SieveAlgo(100)