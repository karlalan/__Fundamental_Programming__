#include <iostream>
using namespace std;

#define ACCURACY 0.000001

signed main()
{
    double num;
    cin >> num;
    if(num<0){
        cout<<"n cannot be negative!"<<endl;
        system("pause");
        return 0;
    }
    if(num==0 || num==1){
        cout<<"Result is "<<num<<endl;
        system("pause");
        return 0;
    }
    double low;
    double high;
    if(num<1){
        low=0;
        high=1;
    }else{
        low=1;
        high=num;
    }
    double mid;
    while(high-low>ACCURACY){
        mid=(high+low)/2;
        if(mid*mid>num){
            high=mid;
        }else{
            low=mid;
        }
    }
    cout<<"Result is "<<low<<endl;
    system("pause");
}