SMALL = 1e-15

def Float32(x):
    return '{0:.32f}'.format(x)

# def Sqrt(n):
#     if n < 0: return 'n cannot be smaller than 0.'
#     if n == 0 or n == 1: return n
#     low, high = 0, 0
#     if n < 1:
#         low = 0; high = 1
#     else:
#         low = 1; high = n
#     while high - low > SMALL:
#         mid = (high+low)/2
#         if mid*mid > n: high = mid
#         else: low = mid
#     return low

# print(Float32(Sqrt(5)))
from decimal import *
import math
# x = Float32()
print(Decimal(math.sqrt(2)))


def SqrtByNewtonMethod(n):
    if n == 0 or n == 1: return n
    root = n/2
    for k in range(100000):
        root = (1/2)*(root+(n/root))
    return root


# y = Float32(SqrtByNewtonMethod(2))
# print(y)
z = Decimal(SqrtByNewtonMethod(2))
print(z)