from sys import stdin, stdout

class Kruskal():
    def __init__(self, graph, costs):
        self._g_ = graph
        self._c_ = costs

    def SubtreeIncludeNode(self, node, Subtree):
        for key, val in Subtree.items():
            if node in key: return key, val

    def Algo(self):
        '''
        Prepare a priority queue including all edges
        '''
        U = [edge[0] for edge in sorted([e for e in self._c_.items()], key = lambda x: x[1])]
        # '''
        # Prepare a subtree including all vertices
        # '''
        S = {node: node for node in list(self._g_.keys())}
        while U:
            i, j = U.pop(0)
            linki, Ti = self.SubtreeIncludeNode(i, S)
            linkj, Tj = self.SubtreeIncludeNode(j, S)
            if Ti != Tj:
                if type(Ti) == dict and type(Tj) == dict:
                    union = {j: i} if i in Ti or i in Tj else {i: j}
                    Ti.update(union)
                    Ti.update(Tj)
                    S.update({linki+linkj: Ti})
                elif type(Ti) == dict and type(Tj) != dict:
                    union = {j: i} if i in Ti else {i: j}
                    Ti.update(union)
                    S.update({linki+i+j: Ti})
                elif type(Ti) != dict and type(Tj) == dict:
                    union = {i: j} if j in Tj else {j: i}
                    Tj.update(union)
                    S.update({linkj+i+j: Tj})
                else:
                    S.update({i+j: {Ti: Tj}})
                del S[linki]
                del S[linkj]
        return list(S.values())


if __name__ == '__main__':
    '''
           A
        /  |  \
       B - C - D
        \ / \ /
         E - F
    '''
    graph = {
        'A': {'B', 'C', 'D'},
        'B': {'A', 'C', 'E'},
        'C': {'A', 'B', 'D', 'E', 'F'},
        'D': {'A', 'C', 'F'},
        'E': {'B', 'C', 'F'},
        'F': {'C', 'D', 'E'},
    }
    costs = {
        'AB': 8, 'AC': 3, 'AD': 1, 'BC': 9, 'BE': 2,
        'CD': 4, 'CE': 7, 'CF': 6, 'DF': 5, 'EF': 10,
    }
    K = Kruskal(graph, costs)
    Res = K.Algo()
    print(Res)
