from sys import stdin, stdout

INF = float('inf')

class Prim():
    def __init__(self, graph, costs, startpoint):
        self._g_ = graph
        self._c_ = costs
        self._start_ = startpoint

    def Algo(self):
        '''
        T: Minimum spanning tree
        C: cost of edge, that is distance
        N: nodes that are closer to current vertex of spanning tree
        '''
        T, C, N = {}, {}, {}
        unvisited = list(self._g_.keys())
        unvisited.remove(self._start_)
        for v in unvisited:
            d = self._c_.get(self._start_+v, self._c_.get(v+self._start_,INF))
            C.update({v: d})
            N.update({v: self._start_})
        while unvisited:
            pCost = list(C.values())
            MinCost = min(pCost)
            w = list(C.keys())[pCost.index(MinCost)]
            unvisited.remove(w)
            del C[w]
            T.update({w: N[w]})
            for v in unvisited:
                d = self._c_.get(w+v, self._c_.get(v+w,INF))
                if d < C[v]:
                    C[v] = d
                    N[v] = w
        return T



if __name__ == '__main__':
    '''
           A
        /  |  \
       B - C - D
        \ / \ /
         E - F
    '''
    graph = {
        'A': {'B', 'C', 'D'},
        'B': {'A', 'C', 'E'},
        'C': {'A', 'B', 'D', 'E', 'F'},
        'D': {'A', 'C', 'F'},
        'E': {'B', 'C', 'F'},
        'F': {'C', 'D', 'E'},
    }
    costs = {
        'AB': 8, 'AC': 3, 'AD': 1, 'BC': 9, 'BE': 2,
        'CD': 4, 'CE': 7, 'CF': 6, 'DF': 5, 'EF': 10,
    }
    P = Prim(graph, costs, 'B')
    Res = P.Algo()
    print(Res)