from sys import stdin, stdout

class QuickSort1():
    def __init__(self, arg):
        self._List_ = arg

    def QuickSort(self, List):
        if len(List) < 2: return List
        pivot = max(List[0:2])
        ## count repetitive pivots
        multiplier = List.count(pivot)
        L = self.QuickSort([x for x in List if x < pivot])
        R = self.QuickSort([x for x in List if x > pivot])
        return L + [pivot]*multiplier + R

    def Main(self):
        return self.QuickSort(self._List_)

    def __str__(self):
        return "Result of QuickSort is: %s" % self.Main()



if __name__ == '__main__':
    target = [4, 5, 1, 3, 7, 1, 10, 2, 3]
    Q1 = QuickSort1(target)
    print(Q1)