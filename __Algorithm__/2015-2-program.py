M = 2**24

def F(n):
    if n < 1: return 1
    def lcg(mod, a, c, seed):
        while True:
            seed = (a*seed + c) % mod
            yield seed
    x, r = lcg(M,161,2457,1), 0
    for i in range(n): r = next(x)
    return r
# print(F(1))

MULTIPLIER = 1103515245
ADDITION = 12345
MOD = 2**26

def G(n):
    if n < 1: return 1
    def lcg(seed):
        while True:
            seed = (MULTIPLIER*seed+ADDITION)%MOD
            yield seed
    x, r = lcg(1), 0
    for i in range(n):
        r = next(x)
        # print(i+1, r)
    return r

print(G(301))


def FibonacciGenerator(n):
    def Exec(n):
        a, b = (1, 1)
        while n > 2:
            a, b = b, a+b
            n -= 1
            yield (a, b)
    f = Exec(n)
    x, y = -1, -1
    for i in range(n-2): x, y = next(f)
    return (x, y)

print(FibonacciGenerator(1000))