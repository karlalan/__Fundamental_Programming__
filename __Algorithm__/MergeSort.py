from sys import stdin, stdout

class MergeSort1():
    def __init__(self, arg):
        self._List_ = arg

    def Merge(self, L, R):
        result = []
        i, j = 0, 0
        L1, L2 = len(L), len(R)
        while i < L1 and j < L2:
            if L[i] < R[j]:
                result.append(L[i])
                i += 1
            else:
                result.append(R[j])
                j += 1
        result.extend(L[i:L1]+R[j:L2])
        return result

    def MergeSort(self,List):
        if len(List) < 2: return List
        mid = len(List) // 2
        L = self.MergeSort(List[:mid])
        R = self.MergeSort(List[mid:])
        return self.Merge(L, R)

    def Main(self):
        return self.MergeSort(self._List_)

    def __str__(self):
        return "Result of MergeSort is: %s" % self.Main()

'''
https://stackoverflow.com/questions/18761766/mergesort-python
'''

class MergeSort2():
    def __init__(self,arg):
        self._List_ = arg

    def MergeSort(self, List):
        if len(List) < 2: return List
        mid = len(List) // 2
        result = []
        L = self.MergeSort(List[:mid])
        R = self.MergeSort(List[mid:])
        while len(L) > 0 and len(R) > 0:
            if L[0] < R[0]: result.append(L.pop(0))
            else: result.append(R.pop(0))
        result.extend(L+R)
        return result

    def Main(self):
        return self.MergeSort(self._List_)

    def __str__(self):
        return "Result of MergeSort is: %s" % self.Main()

if __name__ == '__main__':
    target = [4, 5, 1, 3, 7, 1, 10, 2, 3]
    M1 = MergeSort1(target)
    print(M1)
    M2 = MergeSort2(target)
    print(M2)