from sys import stdin, stdout

INF = float('inf')

class WarshallFloyd():
    def __init__(self, nodes, graph):
        self._nodes_ = nodes
        self._graph_ = graph

    def Dijkstra(self, startpoint):
        unvisited = {node:INF for node in self._nodes_}
        current, currentDis = startpoint, 0
        unvisited[current] = currentDis
        visited = {}
        while True:
            for neighbour, distance in self._graph_[current].items():
                if neighbour not in unvisited: continue
                newDistance = currentDis+distance
                if unvisited[neighbour] > newDistance:
                    unvisited[neighbour] = newDistance
            visited[current] = currentDis
            del unvisited[current]
            if not unvisited: break
            candidates = [node for node in unvisited.items()]
            current, currentDis = sorted(candidates, key = lambda x: x[1])[0]
        return visited

    def WFAlgo(self):
        for eachNode in self._nodes_:
            eachRes = self.Dijkstra(eachNode)
            stdout.writelines("The startpoint is %s. The result of Warshall Floyd Algo is: %s\n" % (eachNode, eachRes))

if __name__ == '__main__':
    '''
    undirected graph
    '''
    nodes = ('A', 'B', 'C', 'D', 'E', 'F', 'G')
    graph = {
        'B': {'A': 5, 'D': 1, 'G': 2},
        'A': {'B': 5, 'D': 3, 'E': 12, 'F' :5},
        'D': {'B': 1, 'G': 1, 'E': 1, 'A': 3},
        'G': {'B': 2, 'D': 1, 'C': 2},
        'C': {'G': 2, 'E': 1, 'F': 1},
        'E': {'A': 12, 'D': 1, 'C': 1, 'F': 2},
        'F': {'A': 5, 'E': 2, 'C': 16}
    }
    '''
    directed graph
    '''
    dnodes = ('s','a','b','c')
    dgraph = {
        's': {'a': 2, 'b': 6},
        'a': {'b': 3, 's': INF},
        'b': {'c': 2, 's': INF},
        'c': {'b': INF}
    }

    WF1 = WarshallFloyd(nodes,graph)
    WF1.WFAlgo()