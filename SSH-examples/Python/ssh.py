from sys import stdout
import paramiko as pk
import os
import openpyxl as opxl
from datetime import datetime
import re

pattern = r"^[0-9]+"

class SSHConnect():
  def __init__(self,hostname,username,password,port):
    self.HostName = hostname
    self.UserName = username
    self.Pass = password
    self.Port = port
    self.MySSH = None
  ## 通信接続
  def Connect(self):
    self.MySSH = pk.SSHClient()
    self.MySSH.load_system_host_keys()
    self.MySSH.set_missing_host_key_policy(pk.AutoAddPolicy())
    IsConnect = 0
    try:
      self.MySSH.connect(hostname=self.HostName,username=self.UserName,password=self.Pass,port=self.Port,timeout=15.0,look_for_keys=False)
      IsConnect = 1
    except pk.AuthenticationException:
      ErrorMessage = "認証失敗です。"
    except pk.SSHException as sshe:
      ErrorMessage = "接続失敗です。\n %s" % sshe
    except pk.BadHostKeyException as bhke:
      ErrorMessage = "hostキー認証失敗です。\n %s" % bhke
    except pk.ssh_exception.NoValidConnectionsError as nvce:
      ErrorMessage = "ipアドレスかportが間違えました。\n %s" % nvce

    if not IsConnect:
      stdout.write( "===========================\n" )
      stdout.write( "[Error]\n%s: %s\n" % (self.HostName,ErrorMessage) )
      stdout.write( "===========================\n" )
    return IsConnect

  def ExecCommand(self,command):
    try:
      ## コマンド実行
      ssh_stdin,ssh_stdout,ssh_stderr = self.MySSH.exec_command(command)
      ## 結果表示
      stdoutRes = ssh_stdout.read()
      if len(stdoutRes):
        result = str(stdoutRes,'utf8').split('\n')
        stdout.write("%s\r\n" % "\r\n".join(result))
        return result
      #   for o in ssh_stdout:
      #     stdout.write("%s\n" % o)

      ##エラーメッセージ
      stderrRes = ssh_stderr.read()
      if len(stderrRes) > 0:
        result = str(stderrRes,'utf8')
        stdout.write("Error: %s\r\n" % result)
        return result
        # for e in ssh_stderr:
        #   stdout.write("Error: %s\n" % e)
    except Exception as e:
      stdout.write( "===========================\n" )
      stdout.write("コマンド実行Error: %s\n" % e)
      stdout.write( "===========================\n" )
  ## デストラクタ
  def __del__(self):
    self.MySSH.close()
########################################################################
####### For txt, csv file
########################################################################
class File():
  def __init__(self, arg):
    self.dir = os.path.dirname( os.path.realpath(__file__) )
    self.filepath = os.path.join( self.dir, "20180702-Yutakabuu/" + arg )
    self.file = None

  def ReadAndWrite( self, content ):
    self.file = open( self.filepath, 'a+' )
    self.file.write( content )

  def GetFileContentAsList( self ):
    self.file = open( self.filepath, 'r' )
    return self.file.read().split('\n')

  def CloseFile( self ):
    self.file.close()
########################################################################
####### For xlsx file
########################################################################
class XlsxFile():
  def __init__(self, arg):
    self.dir = os.path.dirname( os.path.realpath(__file__) )
    self.filepath = os.path.join( self.dir, "20180702-Yutakabuu/" + arg )
    self.wb = None
    self.CurDay = "{0:%Y-%m-%d}".format(datetime.now())

  def IsFileExist(self):
    return os.path.exists(self.filepath)

  def CreateNewFile(self):
    self.wb = opxl.Workbook()

  def LoadXlsxFile(self):
    self.wb = opxl.load_workbook(self.filepath)

  def WriteXlsx(self, contents, server):
    # ws = self.wb.create_sheet(sheetname)
    ws = self.wb.active
    wsPush = ws.append
    if contents is not None:
      for content in contents:
        content = self.MultiReplace(content)
        result = [data for data in content.split(' ') if data != '']
        if len(result):
          if result[0] != self.CurDay:
            if bool(re.match(pattern,result[-1])) and (len(result[-1]) == 4):
              wsPush(result + [server])
    else:
      wsPush( ['No Logs'] )

  def MultiReplace(self, string):
    string = string.replace('-rw-r--r-- 1',"").replace('root','')
    string = string.replace(' 19 ','').replace('+0900','')
    string = string.replace('last_completion_IR_','')
    return string

  def SaveXlsx( self ):
    self.wb.save(self.filepath)
########################################################################
####### Execution
########################################################################
if __name__ == '__main__':
  ## get server list and make a list
  ServerFilePath = 'IR-ServerLists.txt'
  fs = File( ServerFilePath )
  ServerLists = fs.GetFileContentAsList()
  fs.CloseFile()
  ## file object for writing data into file
  DataFilePath = '20180703-LogCollection.xlsx'
  fdata = XlsxFile( DataFilePath )
  if fdata.IsFileExist():
    fdata.LoadXlsxFile()
  else:
    fdata.CreateNewFile()
  ## ssh
  for Server in ServerLists:
    __ssh = SSHConnect(Server,'ss_admin','YsTsK1579',11122)
    stdout.write( "===========================\n" )
    stdout.write( '%s : Connecting....\r\n' % Server )
    stdout.write( "===========================\n" )
    if __ssh.Connect():
      command = 'ls --full-time /home/bin/yutakabuu | grep last'
      result = __ssh.ExecCommand(command)
      fdata.WriteXlsx(result,Server)
  fdata.SaveXlsx()