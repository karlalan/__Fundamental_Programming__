using System;

public class MyEnum{

  enum Color { Blue, Green, Red };

  public static void Main(){

    /**
     * get index of specific element of enum Color
     * @int
     */
    int index = (int)Color.Blue;
    Console.WriteLine( index );
    var val = Color.Blue;
    Console.WriteLine( val );

    /**
     * Get all indices of enum Color
     */
    foreach( int i in Enum.GetValues(typeof(Color)) ){
      Console.WriteLine( i );
    }

    /**
     * Get all values of enum Color
     */
    foreach( var i in Enum.GetValues(typeof(Color)) ){
      Console.WriteLine( i );
    }

    Console.ReadKey();
  }

}