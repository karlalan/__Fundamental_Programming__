#include <iostream>

using namespace std;

enum Color { Blue, Green, Red } MyCol;

enum struct StructCol{ Blue, Green, Red };

const char* EnumString[] = { "Blue", "Green", "Red" };

int main()
{
  /**
   * get index of specific element
   */
  enum Color MyCol;
  MyCol = Blue;
  cout << " index is : " << MyCol << endl;

  /**
   * print the stings of enum elements
   */
  int length = sizeof( EnumString )/sizeof( char* );
  for( int i = 0; i < length; ++i ){
    cout << " The string of enum Color is " << EnumString[i] << endl;
  }

  /**
   * Get all indices
   */
  for( int i = Blue; i <= Red; ++i ){
    cout << " The index of enum Color is " << i << endl;
  }

  ////////////////////////////////////////////////////////////////
  // enum with struct
  int blue =  static_cast<int>(StructCol::Blue);
  cout << " The index of enum struct Color is " << blue << endl;


  return 0;
}