class MyColor{
  enum Color{
    Blue,
    Green,
    RED;
  }
  Color MyFavoriteColor;
}

public class Enum{

  public static void main(String[] args) {

    MyColor col = new MyColor();
    ///////////////////////////////////////////////////////
    //// usage 1
    // col.MyFavoriteColor = MyColor.Color.Blue;
    /////

    ///////////////////////////////////////////////////////
    // usage 2: with function -> valueOf()
    col.MyFavoriteColor = MyColor.Color.valueOf("Blue");
    /////

    ///////////////////////////////////////////////////////
    //// get ordinal
    int ordinal = col.MyFavoriteColor.ordinal();
    /////

    System.out.println( "My Favorite Color is " + col.MyFavoriteColor );
    System.out.println( "My Favorite Color's ordinal is " + ordinal );

  }

}