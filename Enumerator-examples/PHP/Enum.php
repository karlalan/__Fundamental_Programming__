<?php
abstract class Enum{
  private $scalar;

  function __construct( $value ){
    $ref = new ReflectionObject( $this );
    $consts = $ref->getConstants();
    if ( !in_array( $value, $consts, true ) ){
      throw new InvalidArgumentException;
    }

    $this->scalar = $value;
  }

  final public static function __callStatic( $label, $args ){
    $class = get_called_class();
    $const = constant( "$class::$label" );
    return new $class( $const );
  }

  // get value
  final public function valueOf(){
    return $this->scalar;
  }

  final public function __toString(){
    return (string)$this->scalar;
  }

}

///// usage

final class Color extends Enum{
  const Blue = 0;
  const Green = 1;
  const Red = 2;
}

// usage 1
// $color = new Color( Color::Blue );
// usage 2 (after PHP 5.3)
$color = Color::Green();
var_dump( $color );

// get index of specific element
var_dump( $color->valueOf() );
