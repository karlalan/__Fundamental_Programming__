from enum import IntEnum, auto, Flag

class Color(IntEnum, Flag):
  ## auto() will make index from 1 automatically if the index of
  #  first element is not defined
  # Blue = auto()
  Blue = 0
  Green = auto()
  Red = auto()
  Purple = Red | Blue

EnumStringList = [ "Blue", "Green", "Red", "Purple" ]

def getValues( index ):
  return EnumStringList[index]

def valueOf( color ):
  return int( color )



if __name__ == '__main__':
  ###################
  # get string value of enum element
  #####
  print( getValues( Color.Blue ) )
  print( getValues( Color.Purple ) ) #--->due to index = 2

  ###################
  # get index value of enum element
  #####
  print( valueOf( Color.Blue ) )
  print( valueOf( Color.Purple ) )#--->index = 2