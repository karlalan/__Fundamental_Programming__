<?php

///////////////////////////////////////////////////////////
/// dictionary
/// Lots of practices have been done at another file.
/// ------------------------------------------------------------
/// Ref: 2018-php-array functions.php
/// ------------------------------------------------------------
/////////////////////
// initialize an empty array
// usage 1
// $arr = array();
// usage 2
// $arr = [];
// usage 3
$arr = Array();

// add elements to dict
// usage 1
$arr["uno"] = 1;
// usage 2
$arr = array_merge( $arr, array( "dos" => 2 ) );
// usage 3
$arr += array( "tres" => 3 );
$arr += array( "cuatro" => 4 );
// usage 4 but key cannot be assigned
array_push( $arr, "elementAddedWithDefaultKey" );

//delete an element from the end of array
array_pop( $arr );

//delete an element from the head of array
array_shift( $arr );

//add elements from the head of array but key cannot be assigned
array_unshift( $arr, 1 );

print_r( $arr );

// delete all elements
// usage 1
// unset( $arr );
// usage 2
// $arr = NULL;
// usage 3
// $arr = array();
// usage 4
$arr = [];

// length of $arr
var_dump( count( $arr ) ); //--->0 if $arr = NULL

print_r( $arr );//---> $arr becomes an undefined variable if unset( $arr );

$arr["delete"] = 200; //---> if $arr = NULL, array can be used continuously

print_r( $arr );