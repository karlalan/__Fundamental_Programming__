using System;
// data collections
using System.Collections.Generic;
// collections to data structure
using System.Linq;

namespace DataStructure{

  class Program{

    static void Main( string[] args ){
      /////////////////////
      /// fixed size array
      /////////////////////
      int[] num = new int[]{1,2,3,4,5};

      bool[] myBool = new bool[10]; //--->default bools are all false

      char[] c = new char[]{ 'A', 'B', 'C', 'D' };

      // the length of array
      int cLength = c.Length;
      for( int i = 0; i < cLength; ++i ){
        Console.Write( c[i] + " " );
      }
      Console.WriteLine();
      // output ABCD
      Console.WriteLine( c );

      ///////////////////////////////////////////////////////////
      /// list
      /////////////////////
      List<int> numList = new List<int>();
      for( int i = 0; i < 10; ++i ){
        // add elements to dynamic list
        numList.Add( i + 2 );
      }

      // check whether a specific value exists or not
      Console.WriteLine( numList.Contains( 2 ) );

      // reverse the order of elements
      numList.Reverse();

      // update an element by its index
      numList[4] = 999;

      // remove a specific element
      // numList.Remove( 0 );
      // remove elements at range ( x <= i < x+n )
      numList.RemoveRange( 0, 2 );

      // the length of dynamic array
      int numLength = numList.Count;
      for( int j = 0; j < numLength; ++j ){
        Console.Write( "i-th element: " + numList[j] + " " );
      }

      Console.WriteLine();

      // clear all elements
      numList.Clear();
      Console.WriteLine( numList.Count ); //---> length is 0

      List<int> numList2 = new List<int>(){ 3,7,1,5,9 };
      // sort in a ascending way
      // numList2.Sort();
      // sort in a descending way
      numList2.Sort( (a,b) => b - a );
      foreach( int x in numList2 )
        Console.Write( x + " " );

      Console.WriteLine();

      ///////////////////////////////////////////////////////////
      /// Dictionary
      /////////////////////
      Dictionary< string, int > dict = new Dictionary< string, int >();
      dict.Add( "uno", 1 );
      dict.Add( "dos", 2 );
      dict.Add( "tres", 3 );
      dict.Add( "cuatro", 4 );

      // get all keys
      var keyArray = dict.Keys.ToArray();
      foreach( var key in keyArray )
        Console.WriteLine( " My key: " + key );
      // get all values
      var valueArray = dict.Values.ToArray();

      // update value
      dict["uno"] = 100;

      // check whether key or value exists or not
      Console.WriteLine( dict.ContainsKey( "uno" ) );
      Console.WriteLine( dict.ContainsValue( 1 ) );

      // remove an element by its key
      dict.Remove( "cuatro" );

      // order by key ---> dict.OrderBy( key => key.Key ) --->default: ascending
      // order by value ---> dict.OrderBy( key => key.Value ) --->default: ascending

      // key can be used by this way
      // dict ---> dict.Reverse(): reverse the dictionary --->reverse key
      foreach( KeyValuePair< string, int > item in dict )
        Console.WriteLine( "Key = {0}, Value = {1}", item.Key, item.Value );

      // clear all elements
      dict.Clear();
      Console.WriteLine( dict.Count ); //---> length is 0

      ///////////////////////////////////////////////////////////
      /// Tuple : elements cannot be changed
      /////////////////////
      var myTuple = new Tuple<int,int,int,int>(9,8,7,6);

      // access n-th item of Tuple
      Console.WriteLine( myTuple.Item3 ); //---> 7

      var myTuple2 = Tuple.Create("D","A","C","B");
      Console.WriteLine( myTuple2.Item3 ); //---> C



      Console.ReadKey();
    }

  }

}