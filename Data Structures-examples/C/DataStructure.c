#include <stdio.h>
// for using boolean
#include <stdbool.h>
// for using string functions
#include <string.h>

// default boolean array includes only false elements
bool TF[10];

//// number array
int nums[] = { 5,2,4,3,1,6 };
// calculate the length of array
int length = sizeof( nums )/sizeof( int );

//// char array
char* str[] = { "A", "B", "C" };
int sLength = sizeof(str)/sizeof(char*);

char str2[] = "ABC";
char *str3[] = { "A", "B", "C" };

int main(int argc, char const *argv[])
{
  for( int i = 0; i < 10; ++i ){
    printf("%d ", TF[i] ); //---> 0
  }

  printf( "\n" );

  for( int i = 0; i < length; ++i ){
    printf("%d ", nums[i] );
  }

  printf( "\n" );

  for( int i = 0; i < sLength; ++i ){
    printf("%s ", str[i] );
  }

  printf( "\n" );

  printf("%s\n", str2 );

  // calculate string length without pointer
  int len = strlen( str2 ); //---> 3
  int len2 = sizeof( str2 )/sizeof( char ); //---> 4 due to \0 at the end of str2 automatically
  printf("Length is %d\n", len );

  for( int i = 0; i < 3; ++i ){
    printf("%s ", str3[i] );
  }

  // string function
  char string1[] = "Hello";
  char string2[] = "World";
  char string3[] = "";
  // concatenation
  strcat( string1, string2 );
  printf("%s\n", string1 );

  //string copy
  strcpy( string3, string1 );
  printf("%s\n", string3 );

  // return ===> pple
  // single quotation is necessary
  printf("%s\n", strchr( "apple", 'p' ) );


  return 0;
}