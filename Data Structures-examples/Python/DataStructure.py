###########################################################
### list
#####################
## initialize list
List = [ 1, 2, 3, 4, 4, 4 ]
## add new elements to list
List.append( 5 )

## maximum and minimum and summation
print( max( List ) )
print( min( List ) )
print( sum( List ) )

## insert an element at a given position
givenPosition = 3
List.insert( givenPosition, "insert" )

## insert several elements from a given position
List[givenPosition:0] = [99,100]

## print data
print( "List after inserting:" )
print( List )

## calcualte how many the same elements saved in the list
print( "count 4: " + str( List.count( 4 ) ) )

## reverse the list
List.reverse()

## find an element from an assigned position
element = 3
StartingPoint = 2
print( List.index( element, StartingPoint ) )

## remove an element by its value
# if the value does not exist, an error arises
List.remove( "insert" )
print( List )

## sort the list
# sort( key = None, reverse = False ) = sort()
# usage 1
# List.sort( key = None, reverse = False ) ##---> False: ascending/ True: descending
# usage 2
List = sorted( List ) ##---> ascending
print( "Sorting:" )
print( List )

## remove an element at the end of list
List.pop()

## copy a list
# usage 1
# ListCopy = List.copy()
# usage 2
ListCopy = List[:]
print( "ListCopy:" )
print( ListCopy )

## merge lists
L1 = [ "apple", "manual" ]
L2 = [ "banana", "open" ]
# usage 1
# L1.extend( L2 )
# usage 2
L1 += L2
print( "Extend:" )
print( L1 )

## add the same current elements to the same list
L2 *= 3
print( L2 )

## remove all elements
# usage 1
# List.clear()
# usage 2
del List[:]

## the length of list
print( len( List ) )


print( "----------Here is set-----------" )
###########################################################
### set
#####################
## initialize set
# usage 1: can be empty set
set1 = set()
## usage 2: cannot be empty set
set2 = { 1,2,3 } ##---> type: set

print( set2 )

## add elements into set
# it is not in order
set1.add( 12 )
set1.add( 134 )
set1.add( 123 )
set1.add( 999 )
###
# after corret initialization, the add function can be used.
# elsewise, {} will be thought of as dictionary and add function
# can not be used.
set2.add( 4 )

## access an element in set
# [123, 12, 134, 999]
print( list(set1)[0] )

## remove an element by its value
set1.remove( 134 )

## length of set
print( len( set1 ) )

## the maximum and minimum and summation in set
print( max( set1 ) )
print( min( set1 ) )
print( sum( set1 ) )

## sorting
print( sorted( set1 ) ) ##--->type is list

## delete all elements
set1.clear()

print( "----------Here is tuple-----------" )
###########################################################
### Tuple : the elements cannot be changed
#####################
## initialize Tuple
tup1 = ( 5,6,7,8 )
tup2 = ( 1,2,3,4 )
## convert a list to tuple
# tup = tuple(List)

## access an element by its index
print( tup1[2] )

## access elements by a range
StartPoint = 1
EndPoint = 3
print( tup1[StartPoint: EndPoint] ) ##-->(6,7)

## updating tuple
# usage
tup1 += tup2
print( tup1 )

## length of tuple
print( len( tup1 ) )

## maximum and minimum
print( max( tup1 ) )
print( min( tup1 ) )

## result: (1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4)
print( tup2*3 )

## delete a tuple
del tup1

print( "----------Here is dictionary-----------" )
###########################################################
### dictionary
#####################
dict1 = {}
## add elements to dict1
dict1.update( { "name": "Alan" } )
dict1.update( { "age": 12 } )
dict1.update( { "fname": "Chan" } )

## copy a dict
dict2 = dict1.copy()

print( dict1 )

## access an element by its key
# usage 1
print( dict1["name"] )
# usage 2
print( dict1.setdefault("fname") )
# usage 3
print( dict1.get( "age" ) )

## get all items and all keys and all values
print( dict1.items() )
print( dict1.keys() )
print( dict1.values() )

## remove an element by its key
# print( dict1.pop( "age" ) ) ##-->return the deleted value
## remove an element at the end of dict
print( dict1.popitem() )

print( dict1 )

## length of dict
print( len( dict1 ) )

## remove all dict1
# usage 1: empty the dict1
dict1.clear()
# usage 2: the same as unset of php
# del dict1

print( dict1 )


###############
# Create dictionary by using for loop
#######################
ListForDict = ["a","p"]
DictByForLoop = {key:idx for idx,key in enumerate(ListForDict)}
print(DictByForLoop)