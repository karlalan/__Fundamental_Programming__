from array import *

"""
Typecode    Value
  b         Represents signed integer of size 1 byte/td>
  B         Represents unsigned integer of size 1 byte
  c         Represents character of size 1 byte
  i         Represents signed integer of size 2 bytes
  I         Represents unsigned integer of size 2 bytes
  f         Represents floating point of size 4 bytes
  d         Represents floating point of size 8 bytes
"""

##--->all elements of array should be the same type
typecode = 'i'
initializer = [1,2,3,4,5]
arr = array(typecode, initializer)
# for i in arr: print(i)

## insertion
idx, val = 2, 100
arr.insert(idx, val)
arr.append(200)
# for i in arr: print(i)


## deletion operation
tval = 3
arr.remove(tval)
# del arr[0]
# for i in arr: print(i)


## update operation
arr[3] = 9999

## search operation, return index
print(arr.index(9999))
