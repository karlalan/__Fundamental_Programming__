let date = new Date();
// current date
console.log( date.toString() );
// get year
console.log( date.getFullYear() );
// get month
console.log( date.getMonth() + 1 );
// get date
console.log( date.getDate() );
// get day
console.log( date.getDay() );
// get hour
console.log( date.getHours() );
// get minute
console.log( date.getMinutes() );
// get second
console.log( date.getSeconds() );
// get millisecond
console.log( date.getMilliseconds() );
// change to Japanese day
let JPDay = new String( '日月火水木金土' ).charAt( date.getDay() );
console.log( JPDay );

///// Measure execution time
///  there is no sleep function in javascript.....
