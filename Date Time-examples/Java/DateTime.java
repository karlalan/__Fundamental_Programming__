// for using new Date()
// import java.util.Date; it is included below
// for using SimpleDateFormate
import java.util.*;
import java.text.*;
// for measuring the execution of time
import java.lang.*;

public class DateTime{

  public static void main( String[] args ) {
    // Instantiate a Date object
    Date date = new Date();
    // get different units of time
    // if using LocalDateTime, the more precise time can be got
    System.out.println( 1900 + date.getYear() );
    System.out.println( 1 + date.getMonth() );
    System.out.println( 8 + date.getDay() );
    System.out.println( date.getHours() );
    System.out.println( date.getMinutes() );
    System.out.println( date.getSeconds() );

    // display time and date using toString()slpe
    System.out.println( date.toString() );

    //set timezone
    TimeZone tzn = TimeZone.getTimeZone( "Asia/Tokyo" );

    // using simple date format
    SimpleDateFormat ft = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
    ft.setTimeZone( tzn );
    System.out.println( ft.format( date ) );

    // time measurement
    long startTime = System.nanoTime();
    // delay the program
    try{
      // sleep 2 seconds
      Thread.sleep( 2000 );
    }catch( Exception ex ){
      System.out.println( ex );
    }

    long end = System.nanoTime();
    // time difference in nanoseconds
    long diff = end - startTime;
    System.out.println( "Time difference is " + diff + " (ns)" );
  }

}