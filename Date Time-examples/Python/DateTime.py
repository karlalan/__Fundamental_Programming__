from datetime import datetime, timedelta
## for measuring execution time
import time

date = datetime.now()

# ## get year
# print( "Year: " + str( date.year ) )
# ## get month
# print( "Month: " + str( date.month ) )
# ## get day
# print( "day: " + str( date.day ) )
# ## get hour
# print( "hour: " + str( date.hour ) )
# ## get minute
# print( "minute: " + str( date.minute ) )
# ## get second
# print( "second: " + str( date.second ) )
# ## get microsecond
# print( "microsecond: " + str( date.microsecond ) )
# ## get day name
# print( "day name: " + str( date.strftime( "%A" ) ) )


# ##############
# # Measure execution time
# start = time.time()
# ## sleep in unit of second
# time.sleep(1)
# end = time.time()
# print( end - start )

# ##############
# # print out different datetime with the help of timedelta
# #
print( date + timedelta(days=-40) )

year,month,day = 2017,1,2
print(datetime(year,month,day)+timedelta(days=365))