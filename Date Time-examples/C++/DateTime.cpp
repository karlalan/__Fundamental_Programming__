#include <iostream>
// for using time
#include <ctime>
// for using Sleep function in Windows system
#include <windows.h>
// // for using usleep function in Mac system
// #include <unistd.h>

using namespace std;

int main( int argc, char const *argv[] )
{
  time_t currentTime;
  char* c_time_string;
  /**
   * The C library function time_t time(time_t *seconds) returns the
   * time since the Epoch (00:00:00 UTC, January 1, 1970), measured in
   * seconds. If seconds is not NULL, the return value is also stored
   * in variable seconds. Thus, for getting local time, NULL should be
   * set.
   */
  currentTime = time( 0 );
  c_time_string = ctime( &currentTime );
  cout << c_time_string << endl;

  // get all time information by struct tm
  /**
   * The structure of tm.
  struct tm {
        int tm_sec;    秒 (0-60)
        int tm_min;    分 (0-59)
        int tm_hour;   時間 (0-23)
        int tm_mday;   月内の日付 (1-31)
        int tm_mon;    月 (0-11)
        int tm_year;   年 - 1900
        int tm_wday;   曜日 (0-6, 日曜 = 0)
        int tm_yday;   年内通算日 (0-365, 1 月 1 日 = 0)
        int tm_isdst;  夏時間
  };**/
  struct tm *local;
  local = localtime( &currentTime );
  // get year
  int year = local->tm_year + 1900;
  printf("Year %d\n", year );
  // get month
  int month = local->tm_mon + 1;
  printf("Month %d\n", month );
  // get day
  int day = local->tm_mday;
  printf("Day %d\n", day );
  // get hour
  int hour = local->tm_hour;
  printf("Hour %d\n", hour );
  // get minute
  int minute = local->tm_min;
  printf("Minute %d\n", minute );
  // get minute
  int second = local->tm_sec;
  printf("Seconds %d\n", second );
  // get day name
  int day_name = local->tm_wday;
  printf("Day name %d\n", day_name );

  // datetime format
  printf("%04d-%02d-%02d %02d:%02d:%02d\n",
           year, month, day, hour, minute, second );

  //  Measure Execution Time
  clock_t start = clock();
  Sleep( 1000 );
  // usleep( 1000000 ); ////---> unit is microsecond
  clock_t end = clock();
  clock_t milliseconds = end - start;
  cout << "Time difference is " << milliseconds << endl;

  return 0;
}