<?php
/************************************************
 * Inner classes cannot be defined.
 ************************************************/

/************************************************
 * For demonstrating inheritance.
 * Include override.
 ************************************************/
class Area{
  private $Length;
  private $Width;

  function __construct( $l, $w ){
    $this->Length = $l;
    $this->Width = $w;
  }

  public function CalArea(){
    $area = $this->Length * $this->Width;
    var_dump( "Area is " . $area );
    return $area;
  }

  public function DoSomething(){
    var_dump( "I am from Area." );
  }
}

class Volume extends Area{
  private $Length;
  private $Width;
  private $Height;

  function __construct( $l, $w, $h ){
    Parent::__construct( $l, $w );
    $this->Height = $h;
  }

  public function CalVol(){
    $vol = $this->CalArea() * $this->Height;
    var_dump( "Volume is " . $vol );
  }
  // It will override the same name function of parent class.
  public function DoSomething(){
    var_dump( "I am from Volume." );
  }
}

/************************************************
 * For demonstrating abstract class and methods.
 ************************************************/
abstract class Animal{
  abstract protected function GetLegs();
  abstract protected function GetName();
  public function ShowLegs(){
    $name = $this->GetName();
    $legs = $this->GetLegs();
    var_dump( "My name is " . $name );
    var_dump( "My legs is " . $legs );
  }
}

class Dog extends Animal{
  private $Name;
  private $Legs;

  function __construct( $name, $legs ){
    $this->Name = $name;
    $this->Legs = $legs;
  }

  public function GetLegs(){
    return $this->Legs;
  }

  public function GetName(){
    return $this->Name;
  }
}

class Cat extends Animal{
  private $Name;
  private $Legs;

  function __construct( $name, $legs ){
    $this->Name = $name;
    $this->Legs = $legs;
  }

  public function GetLegs(){
    return $this->Legs;
  }

  public function GetName(){
    return $this->Name;
  }
}
/************************************************
 * For demonstrating Encapsulation
 ************************************************/
class Encap{
  private $Name;
  private $Gender;
  private $Age;

  public function SetName( $name ){
    $this->Name = $name;
  }
  public function GetName(){
    return $this->Name;
  }
  public function SetGender( $gender ){
    $this->Gender = $gender;
  }
  public function GetGender(){
    return $this->Gender;
  }
  public function SetAge( $age ){
    $this->Age = $age;
  }
  public function GetAge(){
    return $this->Age;
  }
}
/************************************************
 * For demonstrating Interface.
 ************************************************/
interface ICalc{
  public function calc();
}

class Add implements ICalc{
  private $N1;
  private $N2;
  function __construct( $Num1, $Num2 ){
    $this->N1 = $Num1;
    $this->N2 = $Num2;
  }
  public function calc(){
    var_dump( "The add is " . ( $this->N1 + $this->N2 ) );
  }
}

class Sub implements ICalc{
  private $N1;
  private $N2;
  function __construct( $Num1, $Num2 ){
    $this->N1 = $Num1;
    $this->N2 = $Num2;
  }
  public function calc(){
    var_dump( "The sub is " . ( $this->N1 - $this->N2 ) );
  }
}


/************************************************
 * For Execution
 ************************************************/

/************************************************
 * usage of inheritance including override
 ************************************************/
var_dump("---------Inheritance and Override Demo---------");
$Length = 10;
$Width  = 20;
$Height = 30;

$vol = new Volume( $Length, $Width, $Height );
$vol->CalVol();
$vol->DoSomething();


/************************************************
 * usage of abstract
 ************************************************/
var_dump("---------Abstract Demo---------");
$dName = "puppy";
$dLegs = 4;
$dog = new Dog( $dName, $dLegs );
$cName = "mew";
$cLegs = 4;
$cat = new Cat( $cName, $cLegs );

$animals = array( $dog, $cat );

foreach( $animals as $animal ){
  $animal->ShowLegs();
}

/************************************************
 * usage of Encapsulation
 ************************************************/
var_dump("---------Encapsulation Demo---------");
$name = "Alan";
$gender = "Male";
$age = 18;
$enc = new Encap();
$enc->SetName( $name );
$enc->SetGender( $gender );
$enc->SetAge( $age );
var_dump( "My name is " . $enc->GetName() );
var_dump( "My gender is " . $enc->GetGender() );
var_dump( "My age is " . $enc->GetAge() );

/************************************************
 * usage of Interface
 ************************************************/
var_dump("---------Interface Demo---------");
$num1 = 10;
$num2 = 20;
$add = new Add( $num1, $num2 );
$add->calc();
$sub = new Sub( $num1, $num2 );
$sub->calc();

