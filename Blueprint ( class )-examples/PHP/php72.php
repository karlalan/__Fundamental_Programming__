<?php
// declare(strict_types=1); ////-->if this does not be used, the declaration of type will not be strict.
function sumOfInts(int ...$ints){
    return array_sum($ints);
}
// var_dump(sumOfInts(2.1,3,6));


/**
 * return type declarations
 */
function arraysSum(array ...$arrays): array{
    return array_map(function(array $array): int{
        return array_sum($array);
    },$arrays);
}

// print_r(arraysSum([1,2,3],[4,5,6],[7,8,9.9]));


/**
 * Null coalescing operator
 */
$test = $_GET['aa'] ?? 'nothing';
$test2 = isset($_GET['aa']) ? $_GET['aa'] : 'nothing';
// var_dump($test,$test2);
$y = Null;
$test3 = $x ?? $y ?? 'nothing';
// var_dump($test3);


/**
 * Spaceship operator
 */
$x = 1.5<=>2;
// var_dump($x);


/**
 * constant array
 */
define('Birthdays',[
    'Jan',
    'Feb',
    'Mar'
]);

// var_dump(Birthdays[0]);