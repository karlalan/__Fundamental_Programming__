import java.util.ArrayList;

/************************************************
 * For demonstrating inner and outer classes.
 ************************************************/
class Outer{
  // define inner class
  private class Inner{
    public void sayHi(){
      System.out.println( "---------Inner Class Demo---------" );
      System.out.println( "Hi, I am inner." );
    }
  }

  // define method
  public void CallInnerHi(){
    Inner in = new Inner();
    in.sayHi();
  }
}

/************************************************
 * For demonstrating inheritance.
 * Include override.
 ************************************************/
class Area{
  // define instantiated variables
  private int Length;
  private int Width;
  // constructor
  Area( int l, int w ){
    Length = l;
    Width = w;
  }
  public int CalArea(){
    int area = Length * Width;
    System.out.println( "Area is " + area );
    return area;
  }
  public void DoSomething(){
    System.out.println( "I am from Area!" );
  }
}

class Volume extends Area{
  private int Height;
  Volume( int l, int w, int h ){
    // call the constructor of parent class
    super( l, w );
    Height = h;
  }
  public void CalVol(){
    // usage 1
    // int vol = this.CalArea() * Height;
    // usage 2
    int vol = super.CalArea() * Height;
    System.out.println( "Volume is " + vol );
  }
  // static method cannot be overriden
  @Override
  public void DoSomething(){
    System.out.println( "I am from Volume!" );
  }
}

/************************************************
 * For demonstrating abstract class and methods.
 ************************************************/
abstract class Animal{
  abstract void ShowLegs();
}

class Dog extends Animal{
  private String name;
  private int legs;

  public Dog( String name, int legs ){
    this.name = name;
    this.legs = legs;
  }
  public void ShowLegs(){
    System.out.println( "My name is " + this.name + " and my legs are " + this.legs );
  }
}

class Cat extends Animal{
  private String name;
  private int legs;

  public Cat( String name, int legs ){
    this.name = name;
    this.legs = legs;
  }
  public void ShowLegs(){
    System.out.println( "My name is " + this.name + " and my legs are " + this.legs );
  }
}

/************************************************
 * For demonstrating Encapsulation
 ************************************************/
class Encap{
  private String name;
  private String gender;
  private int age;

  public void SetName( String name ){
    this.name = name;
  }
  public String GetName(){
    return this.name;
  }
  public void SetGender( String gender ){
    this.gender = gender;
  }
  public String GetGender(){
    return this.gender;
  }
  public void SetAge( int age ){
    this.age = age;
  }
  public int GetAge(){
    return this.age;
  }
}

/************************************************
 * For demonstrating Interface
 ************************************************/
interface Calc{
  int NUM1 = 10;
  int NUM2 = 20;

  // define methods
  void calc();
}

class Add implements Calc{
  public void calc(){
    System.out.println( "Add result is " + (NUM1 + NUM2) );
  }
}

class Sub implements Calc{
  public void calc(){
    System.out.println( "Sub result is " + (NUM1 - NUM2) );
  }
}


/************************************************
 * For Execution
 ************************************************/
public class BluePrint{
  public static void main( String[] args ) {
    /************************************************
     * usage of inner and outer classes
     ************************************************/
    // Instantiating the outer class
    Outer o = new Outer();
    // Accessing the CallInnerHi method
    o.CallInnerHi();

    /************************************************
     * usage of inheritance including override
     ************************************************/
    System.out.println("---------inheritance nad override Demo---------");
    int Length = 5;
    int Width  = 15;
    int Height = 10;
    Volume v = new Volume( Length, Width, Height );
    v.CalVol();
    v.DoSomething();

    /************************************************
     * usage of abstract
     ************************************************/
    System.out.println("---------abstract Demo---------");
    ArrayList<Animal> AnimalLists = new ArrayList<Animal>();
    AnimalLists.add( new Dog( "puppy", 4 ) );
    AnimalLists.add( new Cat( "mew", 4 ) );
    for( Animal list : AnimalLists ){
      list.ShowLegs();
    }

    /************************************************
     * usage of Encapsulation
     ************************************************/
    System.out.println("---------Encap Demo---------");
    Encap enc = new Encap();
    enc.SetName( "Alan" );
    enc.SetGender( "Male" );
    enc.SetAge( 18 );
    System.out.println( "My name is " + enc.GetName() );
    System.out.println( "My gender is " + enc.GetGender() );
    System.out.println( "My age is " + enc.GetAge() );

    /************************************************
     * usage of Interface
     ************************************************/
    System.out.println("---------Interface Demo---------");
    Add add = new Add();
    add.calc();
    Sub sub = new Sub();
    sub.calc();


  }
}