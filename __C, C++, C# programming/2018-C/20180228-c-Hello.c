#include <stdio.h>

// Variables declaration
extern int a, b;
const int c = 10;
// ---> it causes only one copy of that member to be shared by all the
// objects of its class
static int d = 1000;

int main( int argc, char *args[] ){
  // printf( "%s\n", "Hello World");

  // // ---> return the bytes of double
  // printf("%d\n", sizeof( double ) );
  // // E is scientific mark
  // printf(" Maximum float positive value: %E\n", FLT_MAX );
  // printf(" Precise value %d\n", FLT_DIG );

  // ---> variable definition
  int a, b;
  // ---> variable initialization
  a = 10;
  b = 5;
  printf("%d\n", a + b + c + d );


  return 0;
}