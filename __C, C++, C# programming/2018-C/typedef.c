#include <stdio.h>
#include <string.h>


/**
 * for eachmember of struct, it only can get 8 bytes as the maximum memory.
 * if the total memories is over the limitation, there is memory destroy
 * caused. This will make the data be distorted.
 */

typedef struct
{
  int id;
  char name[10];
  int balance;
} Bank;

int main(int argc, char const *argv[])
{
  Bank bk;
  bk.id = 1;
  strcpy( bk.name, "Apple" );
  bk.balance = 200;

  printf(" User id is %d\n", bk.id );
  printf(" User name is %s\n", bk.name );
  printf(" User balance %d\n", bk.balance );

  return 0;
}