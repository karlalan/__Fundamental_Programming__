#include <stdio.h>
#include <string.h>

union Data{
  int i;
  float f;
  char str[23];
};

/**
 * The members of union can be saved at the same memory
 * but only one member can be used at one time.
 */

int main(int argc, char const *argv[])
{
  union Data data;

  printf(" Memory occupied by data is %d\n", sizeof(data) );

  data.i = 29;
  printf("%d\n", data.i );

  data.f = 222.22;

  strcpy( data.str, "lslsls" );

  printf(" Memory occupied by data is %d\n", sizeof(data) );


  return 0;
}