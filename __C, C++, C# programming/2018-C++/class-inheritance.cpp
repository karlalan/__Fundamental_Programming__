#include <iostream>

using namespace std;

// base area class
class Area
{
  public:
    Area( double l = 10, double w = 30 ){
      length = l;
      width = w;
    }
    double CalArea(){
      return length * width;
    }

  private:
    double length;
    double width;

};


// derived class
class Volume: public Area
{
  public:
    Volume( double l, double w, double h )
    :Area(l, w){
      height = h;
    };
    double CalVolume(){
      return height * CalArea();
    }

  private:
    double height;

};


int main(){

  Volume V( 3,4,5 );
  cout << " Volume is " << V.CalVolume() << endl;

  return 0;
}

