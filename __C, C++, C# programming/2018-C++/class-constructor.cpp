/**
 * If a copy constructor is not defined in a class,
 * the compiler itself defines one.If the class has pointer variables
 * and has some dynamic memory allocations, then it is a must to
 * have a copy constructor.
 */

#include <iostream>

using namespace std;


class WPF{

  public:
    int getk(void);
    WPF( int k ); //---> simple constructor
    WPF( const WPF &obj ); //---> copy constructor
    ~WPF(); //---> desconstructor

  private:
    int *w;

};

// member functions definitions including constructor
WPF::WPF( int k ){
  cout << " Normal constructor allocating w " << endl;

  // allocate memory for the pointer w
  w = new int;
  *w = k;
}

WPF::WPF( const WPF &obj ){
  cout << " Copy constructor allocating w " << endl;
  w = new int;
  *w = *obj.w; // copy the value
}

WPF::~WPF( void ){
  cout << "Freeing memory" << endl;
  delete w;
}

int WPF::getk( void ){
  return *w;
}

void display( WPF obj ){
  cout << "Here is w " << obj.getk() << endl;
}


int main(){

  WPF wpf(10);
  display( wpf );
  return 0;
}