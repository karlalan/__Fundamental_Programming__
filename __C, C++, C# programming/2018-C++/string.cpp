#include <iostream>

/**
 * the use of cstring class
 * @return [description]
 */
// #include <cstring>
// using namespace std;

// int main(){
//   char str1[10] = "Hello";
//   char str2[10] = "World";
//   char str3[10];
//   int len;

//   //copy str1 into str3
//   strcpy( str3, str1 );
//   cout << " strcpy( str3, str1 ):  " << str3 << endl;
//   // concatenates str1 and str2
//   strcat( str3, str2 );
//   cout << " strcat( str3, str2 ):  " << str3 << endl;
//   // string length
//   len = strlen( str1 );
//   cout << " strlen( str1 ):  " << len << endl;

//   return 0;
// }


/**
 * the use of string class
 */
#include <string>
using namespace std;

int main(){

  string str1 = "Hello";
  string str2 = "World";
  string str3;
  int len;

  // copy str1 into str3
  str3 = str1;
  cout << " str3:  " << str3 << endl;

  // concatenates str3 and str2
  str3 += str2;
  cout << " str3 + str2:  " << str3 << endl;

  // string length
  len = str3.size();
  cout << " string length of str3: " << len << endl;

  return 0;
}