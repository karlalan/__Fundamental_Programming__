#include <iostream>

using namespace std;

class shape{

  protected:
    const double pi = 3.14;
    double length;
    double width;
  public:
    shape( double l, double w ){
      length = l;
      width = w;
    }
    /**
     * Without virtual identifier, it causes static linkage
     */
    // void area(){
    //   cout << "Default area* " << pi * length * width << endl;
    // }

    // virtual void area(){
    //   cout << "Default area* " << pi * length * width << endl;
    // }

    // pure virtual function
    // The = 0 tells the compiler that the function has no body
    // and above virtual function will be called pure virtual function.
    virtual void area() = 0;
};

class Triangle: public shape
{
  public:
    Triangle( int x, int y ):shape( x, y ){}
    void area(){
      cout << "Triangular area* " << 0.5 * length * width << endl;
    }
};

class Rectangle: public shape
{
  public:
    Rectangle( int x, int y ):shape( x, y ){}
    void area(){
      cout << "Rectangle area* " << length * width << endl;
    }
};

int main(){

  shape *s;

  Triangle tri( 2,5 );
  s = &tri;
  s->area();

  Rectangle rec( 2,5 );
  s = &rec;
  s->area();

  return 0;
}