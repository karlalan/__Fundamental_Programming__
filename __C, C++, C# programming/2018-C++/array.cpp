#include <iostream>
using namespace std;

#include <iomanip>
using std::setw;

void MyIntArray(){
  int ArrayInt [10];
  for( int i : ArrayInt ){
    printf("%d\n", i );
  }
  cout << " The third element of ArrayInt: " << ArrayInt[2] << endl;
}


void FormatOutput(){
  int n[10];

  //initialize elements of array n to 0
  for ( int i = 0; i < 10; ++i ){
    n[ i ] = i + 100;
  }
  cout << "Element" << setw( 8 ) << "Value" << endl;
  for ( int j : n ){
    cout << setw(7) << j << setw(8) << j*2 << endl;
  }

}

void CharArray(){
  char hello[] = "HELLO";
  cout << " Say Hello " << endl;
  cout << hello[2] << endl;
}

/**
 * =======================================
 */

int main(){

  // FormatOutput();
  CharArray();

  return 0;
}

/**
 * =======================================
 */









