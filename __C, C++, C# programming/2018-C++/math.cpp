#include <iostream>
#include <cmath>

using namespace std;



/**
 * =======================================
 */

int main(){
  //number definition
  short  s = 10;
  int    i = -1000;
  long   l = 100000;
  float  f = 23.42;
  double d = 123.331;

  // mathematical operations
  cout << " sin(l): " << sin(l) << endl;
  cout << " cos(l): " << cos(s) << endl;
  cout << " abs(i): " << abs(i) << endl;
  cout << " floor(d): " << floor(d) << endl;
  cout << " sqrt(f): " << sqrt(f) << endl;
  cout << " pow(d,2): " << pow(d,2) << endl;

  return 0;
}

/**
 * =======================================
 */


