### find type based on operation system
##--->get the system of target
println( typeof( 1 ) )
println( Int )
println( Sys.WORD_SIZE )
##----> both 32-bit and 64-bit systems
println( typeof( 3*10^8 ) )

## find the maximum and minimum representable numbers of a type
println( typemin( Int ) )
println( typemax( Int ) )

## list the representable range of a type
# Types = [Int8, Int16, Int32, Int64, Int128, UInt8, UInt16, UInt32, UInt64, UInt128]
# for T in Types
#   println( "$(lpad(T,7)): [$(typemin(T)), $(typemax(T))]" )
# end

############
α = 4
println( sizeof( α )/sizeof( Int ) )

## Machine Epsilon
println( eps( Float64 ) )

## Literal zero and one
println( zero( Float64 ) )

println( one( BigFloat ) )


