### Exponetial
println( 3^4 )
println( 2.5e-4 )

### square root
println( sqrt( 9 ) )

## Data structure ######### Vector ##################
# create a row vector
A = [10,20,30]
println( A )
# access a component of row vector
println( A[1] )

# create a column vector
B = [10;20;30]
println( B )

## rounding modes
x = 1.2
y = 0.002
q = x+y
println( q )
setrounding(Float64, RoundUp) do
  println( q )
end

setprecision( 10 ) do
  println( BigFloat( 1 ) + parse( BigFloat, "0.1" ) )
end

## defined constant ###########################
println( pi )
println( 1/Inf )
println( 0*Inf )  ###---> not zero
println( 0.0 )


## Matrix ###########################
# Make a matrix
M = [1 2 3; 4 5 6; 7 8 9]
println( M )

# Access an element
i = 2
j = 1
println( M[i,j] )

# transpose a matrix
println( M' )

# inverse a matrix
N = [1 2; 4 5]
println(inv( N ))

# determine of N
println(det( N ))

# Trace a matrix
println( trace( N ) )

# get eigenvalues
println( eigvals( N ) )

# get eigenvectors
println( eigvecs( N ) )


# divide
println( 4 / 2 )
# inverse divide
println( 4 \ 2 )

# vetorized dot
println( [1,2,3].^3 )

