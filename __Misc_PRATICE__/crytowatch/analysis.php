<?php

$dsn = "mysql:host=localhost;dbname=crytowatch;charset=utf8";
$user = "alan";
$pass = "alan1202";

$dbh = new PDO( $dsn, $user, $pass );

$tables = [ "summary", "orderbook", "price" ];
$results = array();

foreach ( $tables as $table ){

  $data    = getData( $dbh, $table );
  $S_CAMM  = S_CAMM( $dbh, $table );
  $count   = $S_CAMM[0]['COUNT(`cost`)'];
  $avg     = $S_CAMM[0]['AVG(`cost`)'];
  $max     = $S_CAMM[0]['MAX(`cost`)'];
  $min     = $S_CAMM[0]['MIN(`cost`)'];
  $SD      = MakeSD( $data, $count, $avg );
  $results[$table] = array( "count" => $count,
                            "avg"   => $avg,
                            "Max"   => $max,
                            "Min"   => $min,
                            "SD"    => $SD );

}

$CPUTimevol = 8000000000;
echo "毎正時にCPU時間更新です。毎回使えるの量は " . number_format( $CPUTimevol ) . " です。<br/>";
echo "<br>";
echo "一時間内一秒毎にリクエスト回数の計算は <br/>** CPU時間の量/( 必須な量 * 3600 ) **<br/> です。<br/>";
echo "<br>";

foreach ( $results as $target => $result ) {

  echo $target . "のAPIをデータを取得するには <br/>";
  echo "CPU処理時間が更新する前に " . $result['count'] . " のデータ量で以下の計算を行いました。<br/>";
  echo " * 平均値   => " . number_format( $result["avg"], 5 ) . "<br/>";
  echo " CPU time の " . number_format( $result["avg"] / $CPUTimevol, 5 ) . " の量です<br/>";
  echo " !!! 平均値で計算すると一時間内に一秒毎に " . number_format( ReQuestTime( $result["avg"], $CPUTimevol ), 5 ) . " のリクエストができそうです。!!! <br/>";
  echo " * 最大値   => " . number_format( $result["Max"] ) . "<br/>";
  echo " CPU time の " . number_format( $result["Max"] / $CPUTimevol, 5 ) . " の量です<br/>";
  echo " !!! 最大値で計算すると一時間内に一秒毎に " . number_format( ReQuestTime( $result["Max"], $CPUTimevol ), 5 ) . " のリクエストができそうです。!!! <br/>";
  echo " * 最小値   => " . number_format( $result["Min"] ) . "<br/>";
  echo " CPU time の " . number_format( $result["Min"] / $CPUTimevol, 5 ) . " の量です<br/>";
  echo " * 標準偏差 => " . number_format( $result["SD"], 5 )  . "<br/>";
  echo " CPU time の " . number_format( $result["SD"] / $CPUTimevol, 5 ) . " の量です<br/>";
  echo "<br>";

}




function S_CAMM( $dbh, $table ){

  $sql  = " SELECT ";
  $sql .= " COUNT(`cost`), AVG(`cost`), MIN(`cost`), MAX(`cost`) ";
  $sql .= " FROM " . $table;
  $stmt = $dbh->query( $sql );
  return $stmt->fetchAll( PDO::FETCH_ASSOC );

}

function getData( $dbh, $table ){

  $sql  = " SELECT `cost` FROM ";
  $sql .= $table;
  $stmt = $dbh->query( $sql );
  return $stmt->fetchAll( PDO::FETCH_ASSOC );

}

function MakeSD( $data, $count, $avg ){

  $squareSum = 0;
  for( $i = 0; $i < $count; ++$i ){
    $squareSum += pow( $data[$i]["cost"], 2 );
  }
  return sqrt( $squareSum / $count - pow( $avg, 2 ) );

}

function ReQuestTime( $num, $CPUTimevol ){

  return $CPUTimevol / ( $num * 3600 );

}