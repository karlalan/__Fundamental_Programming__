############################
# Bernouli
# 1^k+⋯+n^k modulo 10^9+7
############################
import numpy as np

def MakeDiagonalMatrix( n ):
  return np.diag( [ x for x in range( 1, n+1 ) ] )

def MakeMatrix( k, data, result = [] ):
  if len(result) == 0:
    result = data
  if k > 0:
    result = result.dot( data )
    return MakeMatrix( k - 1, data, result )
  return np.trace( result )

def FindRemainder( n, k ):
  data = MakeDiagonalMatrix( n )
  result = MakeMatrix( k - 1, data )
  quotient, remainder = divmod( result, 10**9+7 )
  return remainder

############################
# Execution
############################

if __name__ == '__main__':

  print( FindRemainder( 100, 4 ) )