question = '''
# ########
# ## ### #
#    ##  #
####    ##
# ## #####
#    #   #
# #### # #
#      # #
## ## ## #
######## #
'''

# question = '''
# ##########
# ##########
# ##########
# ###       
# ### ######
# ### ######
# ### ######
# ### ######
#     ######
# ##########
# '''

# question = '''
# ##########
# ###      #
# ### #### #
# ### #### #
# #   #### #
# # # ####  
# # #      #
# # ########
# #     ####
# ##### ####
# '''

# question = '''
# # ########
# # ########
# # ########
# #       ##
# ### ## ###
# ### ## ###
# #   ######
# # ########
# #       ##
# ####### ##
# '''
# ####################################################
# Define functions 関数定義
# ####################################################
### Readjust list by removing \n 改行コードはいらない
def ReAdjustTo2DArray( question ):
  List = []
  tempList = []
  num = 0
  for element in question:
    if element != "\n":
      if num != 9:
        tempList.append( element )
        num += 1
      else:
        tempList.append( element )
        List.append( tempList )
        tempList = []
        num = 0
  return List

### Switch row and column 配列の行列転置
def Transpose2DArray( List ):
  return list( map( list, zip( *List ) ) )

### Reverse 配列回転
def Reverse2DArray( List, direction ):
  return List[::direction]

### Make a graph for using dfs algorithm
#   深さ優先探索に使うのグラフ作る
#####################################################
def MakeGraph( List ):
  graph = {}
  tempList = []
  Targets = {}
  for row in range( 10 ):
    for column in range( 10 ):
      if List[row][column] == " ":
        if row == 0:
          if List[row][column] == " ":
            Targets['start'] = str( row ) + "," + str( column )
            tempList.append( str( row + 1 ) + "," + str( column ) )
        elif row == 9 or column == 9:
          Targets['goal'] = str( row ) + "," + str( column )
          if row == 9:
            if List[row-1][column] == " ":
              tempList.append( str( row - 1 ) + "," + str( column ) )
          else:
            if List[row][column-1] == " ":
              tempList.append( str( row ) + "," + str( column - 1 ) )
        else:
          if List[row+1][column] == " ":
            tempList.append( str( row + 1 ) + "," + str( column ) )
          if List[row-1][column] == " ":
            tempList.append( str( row - 1 ) + "," + str( column ) )
          if column != 9:
            if List[row][column+1] == " ":
              tempList.append( str( row ) + "," + str( column + 1 ) )
          if column != 0:
            if List[row][column-1] == " ":
              tempList.append( str( row ) + "," + str( column - 1 ) )
      if len( tempList ) > 0:
        graph[str( row ) + "," + str( column )] = set( tempList )
        tempList = []
  Targets["graph"] = graph
  return Targets

### Depth First Search Algorithm 深さ優先探索 ########
#
#  Ref:  http://algoful.com/Archive/Algorithm/DFS  ---> Japanese
#        http://eddmann.com/posts/depth-first-search-and-breadth-first-search-in-python/
#        ---> English
#####################################################
def dfs_paths( graph, start, goal, path = None ):
    if path is None:
        path = [start]
    if start == goal:
        yield path
    for next in graph[start] - set( path ):
        yield from dfs_paths( graph, next, goal, path + [next])

#####################################################
# Execution 実行
#####################################################
question = list( question )
List = ReAdjustTo2DArray( question )

## Check Entrance
IsReverse = False
IsTranspose = False
if " " not in List[0] and " " not in List[-1]:
  List = Transpose2DArray( List )
  IsTranspose = True
elif " " not in List[0]:
  List = Transpose2DArray( List )
  if " " not in List[0]:
    List = Reverse2DArray( Transpose2DArray( List ), -1 )
    IsReverse = True
  else:
    IsTranspose = True
else:
  pass

## Make Graph
graph = MakeGraph( List )

## Find solutions
solutions = list( dfs_paths( graph["graph"], graph["start"], graph["goal"] ) )

## Get the shortest path
if len( solutions ) > 1:
  LenOfSolutions = [ len( path ) for path in solutions ]
  MinPathIndex   = LenOfSolutions.index( min( LenOfSolutions ) )
  ShortestPath   = solutions[ MinPathIndex ]
else:
  ShortestPath = solutions[0]

## Mark each step
startIndices = list( map( int, graph["start"].split( "," ) ) )
for step in ShortestPath:
  stepIndices = list( map( int, step.split( "," ) ) )
  row    = stepIndices[0]
  column = stepIndices[1]
  List[row][column] = "+"

## retranspose array if it has been transposed
if not IsReverse:
  if IsTranspose:
    List = Transpose2DArray( List )
else:
  List = Reverse2DArray( List, -1 )

print( '\n'.join( map( "".join, List ) ) )