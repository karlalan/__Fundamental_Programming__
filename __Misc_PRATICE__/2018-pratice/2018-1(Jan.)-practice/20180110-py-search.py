##########################################
# Split both text and keyword into two
# respective lists and search all words in
# text list with the base of keyword list.
##########################################
# def find_words( text, keyword ):
#   dtext = text.split()
#   dkeyword = keyword.split()

#   found_word = 0

#   for word in dtext:
#     for key_word in dkeyword:
#       if key_word == word:
#         found_word += 1

#   if found_word == len( dkeyword ):
#     return len( keyword )
#   else:
#     return false


# print( find_words( "An apple a day keeps a doctor away", "doctor" ) )

##########################################
# 
##########################################
import re

text = "This text was a test and a trick."
for m in re.finditer( r"\band\b", text ):
  print( m.group(0) )
  if m.group(0):
    print ("Present")
  else:
    print ("Absent")
