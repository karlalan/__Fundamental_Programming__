<?php

var_dump( checkNumber(4.59, false) );

function checkNumber($value, $strict = false)
{
    return ($strict) ? ctype_digit((string)$value) : is_numeric($value);
    // PHP5.1 より前ならこっち
    //return ($strict) ? ($value !== "" && ctype_digit((string)$value)) : is_numeric($value);  
}