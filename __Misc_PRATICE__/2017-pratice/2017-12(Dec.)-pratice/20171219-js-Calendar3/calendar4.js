/**************************************************
 * A class for dealing with calendar.
 **************************************************/
class CALENDAR{

  constructor(){

    this.curDate     = new Date();
    this.year        = this.curDate.getFullYear();
    this.month       = this.curDate.getMonth() + 1;
    this.day         = this.curDate.getDay();
    this.today       = this.curDate.getDate();
    this.weeknth     = [ "1w", "2w", "3w", "4w", "5w", "6w" ];
    this.day_of_week = [ "Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat" ];
    this.month_en    = { 1:"January",
                         2:"Feburary",
                         3:"March",
                         4:"April",
                         5:"May",
                         6:"June",
                         7:"July",
                         8:"August",
                         9:"September",
                        10:"October",
                        11:"November",
                        12:"December"
                       };

  }

  GetMonthTitle( M ){

    return this.month_en[M];

  }

  GetMonthTitleKey( title ){

    for( var key in this.month_en ){
      if( this.month_en[key] === title ){
        return key;
      }
    }

  }

  GetDaysInFeb( year ){

    let days_in_Feb = ( year % 4 == 0 && year % 100 != 0 ) ? 29 : 28;
    if( year % 400 == 0 ){ days_in_Feb = 29; }
    return days_in_Feb;

  }

  Cal_days_in_month( Y, M ){

    let cal_days_in_month = { 0:31,//for 1 - 1 = 0
                              1:31,
                              2:this.GetDaysInFeb( Y ),
                              3:31,
                              4:30,
                              5:31,
                              6:30,
                              7:31,
                              8:31,
                              9:30,
                              10:31,
                              11:30,
                              12:31
                            };
    return cal_days_in_month[M];

  }

  /**
   * Create table cells for day-of-week.
   */
  MakeCalendar( Y, M ){

    this.WriteCalHead();
    this.RecordYM( Y, M );
    this.WriteCalendar( Y, M );

  }//End of MakeCalendar

  SwitchCalendar( clickedSpanId ){

    let action   = clickedSpanId,
        curYear  = this.GetYear(),
        curMonth = this.GetMonth(),
        Y, M;
    document.getElementById( "day-of-week" ).lastChild.remove();
    $( "#day" ).find( "tr" ).remove();
    CALENDAR.DelRecord();
    if( action == "prev" ){
      switch( curMonth ){
        case 1:
          Y = curYear - 1;
          M = 12;
          break;
        default:
          Y = curYear;
          M = curMonth - 1;
          break;
      }
    } else {
      switch( curMonth ){
        case 12:
          Y = curYear + 1;
          M = 1;
          break;
        default:
          Y = curYear;
          M = curMonth + 1;
          break;
      }
    }
    this.MakeCalendar( Y, M );

  }// End of switch calendar

  WriteCalHead(){

    let MulSpan  = ELEM.MakeMultipleElem( "span", 4 ),
        table = ELEM.MakeHTMLElem( "table" ),
        thead = ELEM.MakeHTMLElem( "thead" ),
        MulTh = ELEM.MakeMultipleElem( "th", 4 ),
        MulTr = ELEM.MakeMultipleElem( "tr", 2 ),
        tbody = ELEM.MakeHTMLElem( "tbody" ),
        divContainer = document.getElementsByClassName( "Alan-calendar" )[0],
        i, th, span;
    //arrow prev
    MulSpan[0].id = "prev";
    MulSpan[0].innerHTML = "&laquo;";
    MulTh[0].appendChild( MulSpan[0] );
    //Year and Month
    MulSpan[1].id = "cal-year";
    MulTh[1].colSpan = 2;
    MulTh[1].appendChild( MulSpan[1] );
    MulSpan[2].id = "cal-month";
    MulTh[2].colSpan = 3;
    MulTh[2].appendChild( MulSpan[2] );
    //arrow next
    MulSpan[3].id = "next";
    MulSpan[3].innerHTML = "&raquo;";
    MulTh[3].appendChild( MulSpan[3] );
    //head info
    MulTr[0].id = "cal-info";
    for( i = 0; i < 4; ++i ){
      MulTr[0].appendChild( MulTh[i] );
    }
    //table
    table.id = "cal-table";
    thead.id = "cal-head";
    tbody.id = "cal-body";
    table.appendChild( thead );
    table.appendChild( tbody );
    divContainer.appendChild( table );
    for( i = 0; i < 7; ++i ){
      th = ELEM.MakeHTMLElem( "th" );
      span = ELEM.MakeHTMLElem( "span" );
      span.innerHTML = this.day_of_week[i] + ".";
      if( i == 0 || i == 6 ){
        span.style.color = "red";
      }
      th.appendChild( span );
      MulTr[1].appendChild( th );
    }
    for( i = 0; i < 2; ++i ){
      thead.appendChild( MulTr[i] );
    }

  }

  WriteCalendar( Y, M ){

    let last_day_this_month  = new Date( Y, M - 1, this.Cal_days_in_month( Y, M ) ).getDay(),
        idYear  = ( ( M + 1 ) > 12 ) ? ( Y + 1 ) : ( Y ),
        idMONTH = ( ( M + 1 ) > 12 ) ? ( 1 ) : ( M + 1 );
    this.WriteFirstWeek( Y, M );

  }

  WriteFirstWeek( Y, M ){

    let top_tbody = document.getElementById( "cal-body" ),
        first_day_this_month = new Date( Y, M - 1, 1 ).getDay(),
        days_prev_month = this.Cal_days_in_month( Y, M - 1 ),
    //for first week
        tablew1 = ELEM.MakeHTMLElem( "table" ),
        theadw1 = ELEM.MakeHTMLElem( "thead" ),
        tbodyw1 = ELEM.MakeHTMLElem( "tbody" ),
        MulTr = ELEM.MakeMultipleElem( "tr", 3 ),
        i;
    MulTr[0].id  = this.weeknth[0];
    MulTr[1].classList.add( "todo-list" );
    for( i = 0; i < 7; ++i ){
      let th   = ELEM.MakeHTMLElem( "th" ),
          td   = ELEM.MakeHTMLElem( "td" ),
          span = ELEM.MakeHTMLElem( "span" ),
          day_in_prev_month = days_prev_month - ( first_day_this_month - 1 - i );
      if( i < first_day_this_month ){
        if( ( M - 1 ) == 0 ){
          span.id = ( Y - 1 ) + "-" + 12 + "-" + day_in_prev_month;
          td.id = span.id + "list";
        } else {
          span.id = Y + "-" + ( M - 1 ) + "-" + day_in_prev_month;
          td.id = span.id + "list";
        }
        span.style.color = "rgba(192,192,192,0.5)";
        span.innerHTML = day_in_prev_month;
      } else {
        span.id = Y + "-" + M + "-" + ( i - first_day_this_month + 1 );
        td.id = span.id + "list";
        span.classList.add( "active" );
        span.innerHTML = ( i - first_day_this_month + 1 );
        if( i == 0 || i == 6 ){
          span.style.color = "red";
        }
        if( ( i - first_day_this_month + 1 ) == this.today && M == this.month && Y == this.year ){
          th.style.backgroundColor = "lightgreen";
        }
      }
      span.style.cursor = "pointer";
      th.appendChild( span );
      MulTr[0].appendChild( th );
      MulTr[1].appendChild( td );
    }
    theadw1.appendChild( MulTr[0] );
    tbodyw1.appendChild( MulTr[1] );
    tablew1.appendChild( theadw1 );
    tablew1.appendChild( tbodyw1 );
    MulTr[2].appendChild( tablew1 );
    MulTr[2].colSpan = 7;
    top_tbody.appendChild( MulTr[2] );

  }

  GetYear(){

    let year = Number( $( "#cal-year" ).text() );
    if( !year ){ year = this.year; }
    return year;

  }

  GetMonth(){

    let month = Number( this.GetMonthTitleKey( $( "#cal-month" ).text() ) );
    if( !month ){ month = this.month; }
    return month;

  }

  RecordYM( Y, M ){

    $( "#cal-year" ).text( Y );
    $( "#cal-month" ).text( this.GetMonthTitle( M ) );

  }

  static DelRecord(){

    $( "#cal-year" ).text( "" );
    $( "#cal-month" ).text( "" );

  }

}//End of Class


/**************************************************
 * A class for dealing with html element via JS.
 **************************************************/
class ELEM{

  static MakeHTMLElem( ELEM ){

    return document.createElement( ELEM );

  }

  static MakeMultipleElem( ELEM, num ){

    let elem = {}, i;
    for( i = 0; i < num; ++i ){
      elem[i] = document.createElement( ELEM );
    }
    return elem;

  }

}