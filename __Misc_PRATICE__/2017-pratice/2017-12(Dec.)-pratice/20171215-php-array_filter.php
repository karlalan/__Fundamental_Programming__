<?php
###There is callback.
function odd( $var ){
  return ( $var & 1 );
}

function even( $var ){
  return !( $var & 1 );
}

$array1 = array("a"=>1, "b"=>2, "c"=>3, "d"=>4, "e"=>5);

print_r( array_filter( $array1, "odd" ) );

print_r( array_filter( $array1, "even" ) );

$array2 = array(6, 7, 8, 9, 10, 11, 12);

print_r( array_filter( $array2, "even" ) );

###There is flag.
$arr = ['a' => 1, 'b' => 2, 'c' => 3, 'd' => 4];

var_dump( array_filter( $arr, function( $k ) {
    return $k == 'b';
}, ARRAY_FILTER_USE_KEY ) );

var_dump(array_filter( $arr, function( $v, $k ) {
    return $k == 'b' || $v == 4;
}, ARRAY_FILTER_USE_BOTH ) );


###There is no callback
# This will delete a value that (bool)value == false
$entry = array(
             0 => 'foo',
             1 => false,
             2 => -1,
             3 => null,
             4 => ''
          );

print_r( array_filter( $entry ) );


##################filter_var
$ip = "2001:0db8:85a3:08d3:1319:8a2e:0370:7334";

// Validate ip as IPv6
if (!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) === false) {
    echo("$ip is a valid IPv6 address");
} else {
    echo("$ip is not a valid IPv6 address");
}