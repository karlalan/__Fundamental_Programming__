var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  $( function() {
    let slides = $( ".slideshow-container" ).children( "div" ),
        dots = $( "#dot" ).children( "span" );
    if ( n > slides.length ) { slideIndex = 1; }
    if ( n < 1 ) { slideIndex = slides.length; }
    for ( var i = 0; i < slides.length; ++i ) {
        if( i == ( slideIndex - 1 ) ){
          slides[i].style.display = "block";
        } else {
          slides[i].style.display = "none";
        }
    }
    for ( var i = 0; i < dots.length; ++i ) {
        if( i == ( slideIndex - 1 ) ){
          dots[slideIndex-1].className += " active";
        } else {
          dots[i].className = dots[i].className.replace(" active", "");
        }
    }
  });
}