<?php

require_once( "mail/mail.php" );

$user = $_POST["user_id"];
$pass = $_POST["user_password"];

if( !empty( $user ) && !empty( $pass ) ){
  $Sender = "s-himegi@sitescope.co.jp";
  $SenderName = "Himegi";
  $MAILIER = new MAILIER( $Sender, $SenderName );
  $Recipients = array();
  $Recipients[0]["name"]     = $SenderName;
  $Recipients[0]["mailaddr"] = $Sender;

  $Subject = "Confirm Email";
  $MAILIER->MakeSubject( $Subject );

  $Body = rand( 1, 100 );
  setcookie( "token", $Body, time()+10 );
  $MAILIER->MakeBody( $Body );
  if( $MAILIER->SendMail() === "ok" ){
    header( "Content-Type: text/html; charset=utf8" );
    header( "Location: token.html" );
  } else {
    echo "Mail Error";
    exit();
  }

} else {
  header( "Location: signin.html" );
  exit();
}