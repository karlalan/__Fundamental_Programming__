/*********************************************
 * Class定義
 *********************************************/
class Catagory{

  /**
   * ブロックのタイトル
   */
  static Blocks(){
    return [ "MarketCap", "Volume", "Price", "Change" ];
  }
  /**
   * テーブルのマスを作る
   */
  static MakeBlockCells(){

    Catagory.Blocks().forEach( function( Block ) {
        for( var i = 0; i < 10; ++i ){
          var tr       = document.createElement( "tr" ),
              img      = document.createElement( "img" ),
              aLink    = document.createElement( "a" ),
              spanImg  = document.createElement( "span" ),
              spanStr  = document.createElement( "span" ),
              tdRank   = document.createElement( "td" ),
              tdName   = document.createElement( "td" ),
              tdBlock  = document.createElement( "td" ),
              bNode = document.getElementById( Block + "_block" ),
              pNode = bNode.parentNode;
          //rank
          tdRank.id = i + "_" + Block;
          tr.appendChild( tdRank );
          //coin name and logo
          img.id  = i + "_logo_" + Block;
          spanImg.appendChild( img );
          tdName.appendChild( spanImg );
          aLink.id = i + "_link_" + Block;
          aLink.target = "_blank";
          spanStr.id = i + "_coin_" + Block;
          aLink.appendChild( spanStr );
          tdName.appendChild( aLink );
          tr.appendChild( tdName );
          //Block
          tdBlock.id = i + "_data_" + Block;
          tr.appendChild( tdBlock );
          //insert cells
          pNode.insertBefore( tr, null );
        }
    });

  }
  /**
   * ブロックによってそれぞれのデータを表示する
   * @param object BlocksData APIで取得してソートしてブロックに振り分けたデータ
   */
  static MakeCoinsList( BlocksData ){

    Object.keys( BlocksData ).forEach( function( Block ) {
        for( var i = 0; i < CoinsNum; ++i ){
          var trID      = Block + "_" + BlocksData[Block][i].coin,
              ExistData = $( "#" + trID ).children().eq(2).text(),
              NewData   = BlocksData[Block][i].data;
          if( ExistData ){
            ExistData = Catagory.RepMarks( ExistData );
            if( Block == "Price" ){ NewData = Catagory.FloatFormat( NewData, 4 ); }
            if( ExistData > NewData ){
              Catagory.HighLight( $( "#" + i + "_" + Block ).closest( "tr" ), 360, 80, 75, 1000, 200 );
            } else if( ExistData < NewData ){
              Catagory.HighLight( $( "#" + i + "_" + Block ).closest( "tr" ), 145, 70, 70, 1000, 200 );
            } else {
            }
          }
          $( "#" + i + "_" + Block ).closest( "tr" ).attr( "id", trID );
          $( "#" + i + "_" + Block ).text( (i + 1) );
          $( "#" + i + "_logo_" + Block ).attr( "src", Catagory.GetImg( BlocksData[Block][i].coin ) );
          $( "#" + i + "_link_" + Block ).attr( "href", Catagory.GetLink( BlocksData[Block][i].id ) );
          $( "#" + i + "_coin_" + Block ).html( " " + BlocksData[Block][i].coin );
          if( Block == "Change" ){
            ( BlocksData[Block][i].data < 0  ) ? $( "#" + i + "_data_" + Block ).css( "color", "red" ) : $( "#" + i + "_data_" + Block ).css( "color", "green" );
            $( "#" + i + "_data_" + Block ).text( BlocksData[Block][i].data + " %" );
          } else {
            $( "#" + i + "_data_" + Block ).html( "$ " + Catagory.AllNumFormat( Block, NewData ) );
          }
        }
    });

  }
  /**
   * 表示させたいデータをブロックによってまとめる。
   * @param object CoinsData APIから取得したデータ
   */
  static MakeBlockData( CoinsData ){

    var BlocksData = {},
        MktCapCond = SORT.GetDataTableSortCond( "MarketCap" ),
        VolumeCond = SORT.GetDataTableSortCond( "Volume" ),
        PriceCond  = SORT.GetDataTableSortCond( "Price" ),
        ChangeCond = SORT.GetDataTableSortCond( "Change" );
    this.Blocks().forEach( function( Block ) {
        BlocksData[Block] = [];
    });
    for( var i = 0; i < CoinsNum; ++i ){
      //MarketCap
      BlocksData["MarketCap"][i] = { "coin" : CoinsData[i].symbol,
                                     "id"   : CoinsData[i].id,
                                     "data" : Math.floor( Number( CoinsData[i]["market_cap_usd"] ) ) };
      //Volume
      BlocksData["Volume"][i] = { "coin" : CoinsData[i].symbol,
                                  "id"   : CoinsData[i].id,
                                  "data" : Math.floor( Number( CoinsData[i]["24h_volume_usd"] ) ) };
      //Price
      BlocksData["Price"][i] = { "coin" : CoinsData[i].symbol,
                                 "id"   : CoinsData[i].id,
                                 "data" : Number( CoinsData[i]["price_usd"] ) };
      //Change
      BlocksData["Change"][i] = { "coin" : CoinsData[i].symbol,
                                  "id"   : CoinsData[i].id,
                                  "data" : Number( CoinsData[i]["percent_change_24h"] ) };
    }
    SORT.RankSort( BlocksData["MarketCap"], MktCapCond["column"], MktCapCond["order"] );
    SORT.RankSort( BlocksData["Volume"], VolumeCond["column"], VolumeCond["order"] );
    SORT.RankSort( BlocksData["Price"], PriceCond["column"], PriceCond["order"] );
    SORT.RankSort( BlocksData["Change"], ChangeCond["column"], ChangeCond["order"] );
    Catagory.MakeCoinsList( BlocksData );

  }
  /**
   * ajaxでphpへ要求してデータを取得する。
   * @return JSON APIから取得したデータ
   */
  static getCoinsData(){

    $.ajax({
        type: "GET",
        url:  "/api_coincap_catagory/",
        data: { "CoinsNum": CoinsNum },
        dataType: "json",
        success: function( json ){
          Catagory.MakeBlockData( json );
        },
        error: function( XMLHttpRequest, textStatus, errorThrown ) {
            $( "#dataErr" ).text( "データエラー or APIエラー" );
        }
    });
  }
  /**
   * H: hue, S: saturation, L:lightness, MS: milisecond
   * 色の参考サイト: https://www.w3schools.com/colors/colors_hsl.asp
   * 最初:hsl( 360, 80%, 75% )--->赤色
   *      hsl( 145, 70%, 70% )--->緑色
   */
  static HighLight( Target, H, S, L, MS, Speed ){
    //最初の色をつける
    Target.css( "backgroundColor", "hsl(" + H + "," + S + "%," + L + "%)" );
    //色を徐々に薄くする
    var Time = MS;
    for( var i = L; i <= 100; ++i ){
      Time += Speed;
      ( function( L, Time ) {
        setTimeout( function() {
          Target.css( "backgroundColor", "hsl(" + H + "," + S + "%," + i + "%)" );
        }, Time);
      })( L, Time );
    }

  }

  /**
   * 数字のフォーマットやリンクやイメージやを処理する様々な関数
   */
  static IntFormat( IntNumber ){ return new Intl.NumberFormat().format( IntNumber ); }
  static FloatFormat( FloatNum, n ){ return parseFloat( FloatNum ).toFixed( n ); }
  static AllNumFormat( Block, data ){

    switch( Block ){
      case "MarketCap":
      case "Volume":
         return Catagory.IntFormat( data );
         break;
      case "Price":
         return Catagory.FloatFormat( data , 4 );
         break;
    }

  }
  //url
  static BaseLink(){ return "https://coinmarketcap.com/currencies/"; }
  static GetLink( CoinId ){ return this.BaseLink() + CoinId; }
  static GetIDFromLink( Url ){ return Url.replace( this.BaseLink() , "" ); }
  //Img base path
  static BasePath(){ return "{$smarty.const.PATH_TOP_RELATIVE}dist/img/coinLogo/"; }
  static GetImg( CoinName ){ return this.BasePath() + CoinName + ".png"; }
  //記号をスペースで入れ替える
  static RepMarks( StrData ){

    StrData = StrData.replace( "$ ", "" );
    StrData = StrData.replace( " %", "" );
    StrData = StrData.replace( /,/g, "" );
    return Number( StrData );

  }

}