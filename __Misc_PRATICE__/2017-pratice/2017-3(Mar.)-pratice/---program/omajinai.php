<?php

function init(){
    ini_set("output_buffering","on");
    ini_set("output_handler","mb_output_handler");
    ini_set("default_charset","EUC-jp");
    mb_language("Japanese");
    mb_internal_encoding ("EUC-JP");
    mb_http_input("auto");
    mb_http_output("EUC-JP");
    mb_detect_order("auto");
    mb_substitute_character("none");
    ob_start("mb_output_handler");

    header("Content-Type: text/html; charset=EUC-JP");
    session_start();
}
?>