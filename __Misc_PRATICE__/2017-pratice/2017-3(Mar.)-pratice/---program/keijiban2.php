 <?php
//関数ファイルを読み込む
require_once("function.php");

//初期設定関数の呼び出し
//init();

//エラーで戻ってきた場合で保存していた値をフォームに表示する
$name = htmlspecialchars($_SESSION["name"],ENT_QUOTES);
$title = htmlspecialchars($_SESSION["title"],ENT_QUOTES);
$body = htmlspecialchars($_SESSION["body"],ENT_QUOTES);

//エラー情報をクリアする
$error_mes = $_SESSION["error_mes"];
$_SESSION["error_mes"] = "";

// HTMLを出力する（書き込み部分
?>