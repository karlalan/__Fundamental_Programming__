// $(window).on( "load", function() {
//   GetBitBoard();
//   GetCoinBoardData();
// });

function GetBitBoard() {
  setInterval( function() {
    $.ajax({
      type: "GET",
      url:  "/php/BitFlyer.php",
      dataType: "json",
      // success: function( json ){

      // },
      error: function( XMLHttpRequest, textStatus, errorThrown ) {
        $( "#err" ).text( "Error" );
      }
    });
  }, 1000 );
}

function GetCoinBoardData(){
  setInterval( function() {
    $.ajax({
      type: "GET",
      url:  "/php/CoinCheck.php",
      dataType: "json",
      // success: function( json ){

      // },
      error: function( XMLHttpRequest, textStatus, errorThrown ) {
        $( "#err" ).text( "Error" );
      }
    });
  }, 1000 );
}

function DisplayData(){
  setInterval( function() {
    $.ajax({
      type: "GET",
      url:  "",
      dataType: "json",
      success: function( json ){
        nv.addGraph( function() {
          var chart = nv.models.candlestickBarChart()
          .x( function( d ) { return d["date"] })
          .y( function( d ) { return d["close"] })
          .duration( 250 )
          .margin( { left: 75, bottom: 50 } );

          chart.xAxis.axisLabel( "時間" ).tickFormat( function( d ) {
            return d3.time.format( "%H:%M:%S" )( new Date() );
          });

          chart.yAxis.axisLabel( "株価" ).tickFormat( functionvn( d, i ) {
            return "￥" + d3.format( ',.2f' )( d );
          });

          d3.select( "#StockPrice svg" ).datum( json ).transition().duration( 500 ).call( chart );
          nv.utils.windowResize( chart.update );
          return chart;

        });
      },
      error: function( XMLHttpRequest, textStatus, errorThrown ){
        $( "#displayErr" ).text( "データエラー" );
      }
    });
  }, 1000);
}