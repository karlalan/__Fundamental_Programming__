<?php
include_once 'db/DB.php';
$path = "https://coincheck.com/api/order_books";
$results = file_get_contents( $path );
$results = json_decode( $results, true );

if( !empty( $results ) ) {
  exit( json_encode( $results ) );
}