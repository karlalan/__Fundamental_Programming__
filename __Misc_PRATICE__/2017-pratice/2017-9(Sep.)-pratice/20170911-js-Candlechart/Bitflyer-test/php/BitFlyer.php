<?php
$path = "https://lightning.bitflyer.jp/v1/getboard";
$results = file_get_contents( $path );
//it can change data type from object to array by using second parameter "true" of json_decode
$results = json_decode( $results, true );
//-----> remove "mid_price"
$splice_results = array_splice( $results, 0, 1);

$bids = $results["bids"];
$bids_high = max( $bids )["price"];
$bids_low  = min( $bids )["price"];

$asks = $results["asks"];
$asks_high = max( $asks )["price"];
$asks_low  = min( $asks )["price"];

include_once 'db/DB.php';
// $sql = " INSERT INTO bitflyer( open,high,low,close ) VALUES ( :open,:high,:low,:close ) ";

// $stmt = $dbh->prepare( $sql );
// $stmt->bindvalue( ':open' ,$bids_high );
// $stmt->bindvalue( ':high', $asks_high );
// $stmt->bindvalue( ':low', $asks_low );
// $stmt->bindvalue( ':close', $bids_low );
// try{
//   $stmt->execute();
// }catch( PDOException $e ){
//   $e->getMessage();
// }



$values = [$bids_high, $asks_high, $asks_low, $bids_low];
$SaveData = new DB();
$results = $SaveData->ConnectionSave( $values );

// if( !empty( $results ) ) {
//   var_dump( $bids_high );
//   var_dump( $asks_high );
// }