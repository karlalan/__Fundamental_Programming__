<?php

function checkNumber ($value, $strict = false) {
    return ($strict) ? ctype_digit((string)$value) : is_numeric($value);
}

var_dump (checkNumber (323,$strict = true));


function checkNumber2 ($value, $strict = false) {
    if ($strict) {
        return $strict = ctype_digit((string)$value);
    } else {
        return $strict = is_numeric($value);
    }
}

var_dump (checkNumber2 (323,$strict = true));