<?php
$List = ["1","2","3","1","2","3"];

//----for loop----
function for_tot (array $calculated) {
    $result = "";
    for ($i = 0; $i < count($calculated); $i++) {
        $result += $calculated[$i];
    }
    print "for: ".$result.PHP_EOL;
}

for_tot ($List);

//-----while loop----
function while_tot (array $calculated) {
    $result = "";
    $i = 0;
    while ($i < count($calculated)) {
        $result += $calculated[$i];
        $i++;
    }
    print "while: ".$result.PHP_EOL;
}

while_tot ($List);

//--foreach ----
function foreach_tot (array $calculated) {
    $result = "";
    foreach ($calculated as $key => $value) {
        $result += $value;
    }
    print "foreach: ".$result.PHP_EOL;
}

foreach_tot ($List);

//--recursive function--
function rec ( $sum, array $calculated, $i ){
    $length = count ( $calculated );
    if ($i >= $length){
        return false;
    }
    $sum += $calculated[$i] + rec ($sum, $calculated, $i + 1);
    return $sum;
}

echo "recursive function: ".rec(0,$List,0);
