<?php

/**
 * call_user_func_arrayのラッパー
 * テーブルデータのような二次元配列を任意の項目(配列のキー)でソートする
 *
 * arg1 対象の配列 参照渡しなので直接変更する
 * arg2 一つまたは複数のソート条件
 *      [ [ キー(文字列であること) , SORT_ASC/SORT_DESC(省略時はASC) , SORT_REGULAR/SORT_NUMERIC/SORT_STRING/ほか(省略時はREGULAR)※array_multisort参照 ],
 *        [ 上に同じ ], ... ]
 *      ソート条件が一つの場合は配列にしなくても可 [キー, SORT_ASC, SORT_REGULAR]単体で
 *      キーだけなら配列にしなくても可
 * return true / false
 *
 * 例
 * array_order($arr, [['key1', SORT_DESC], ['key2', SORT_DESC]]);
 * array_order($arr, ['key1', ['key2', SORT_DESC]]);
 * array_order($arr, ['key2', SORT_DESC]); 単体指定
 * array_order($arr, 'key2', SORT_DESC); これでも可
 */
function array_order(&$array, $order)
{
    // オーダー指定部部分を調整するだけ
    if (is_string($order)) {
        $argc = func_num_args();
        if ($argc == 2) {
            $order2 = array(array($order));
        } elseif ($argc == 3) {
            if (is_int(func_get_arg(2))) {
                $order2 = array(array($order, func_get_arg(2)));
            } else {
                return false;
            }
        } elseif ($argc > 3) {
            if (is_int(func_get_arg(2)) && is_int(func_get_arg(3))) {
                $order2 = array(array($order, func_get_arg(2), func_get_arg(3)));
            } else {
                return false;
            }
        } else {
            return false;
        }
    } elseif (!is_array($order)) {
        return false;
    } else {
        if (empty($order)) {
            return true;
        }
        if (count($order) == 1 && is_string($order[0]) ||
            count($order) == 2 && is_string($order[0]) && is_int($order[1]) ||
            count($order) == 3 && is_string($order[0]) && is_int($order[1]) && is_int($order[2]))
        {
            $order2 = array($order);
        } else {
            $order2 = array();
            foreach ($order as $x) {
                if (is_array($x)) {
                    if (count($x) == 1 && is_string($x[0]) ||
                        count($x) == 2 && is_string($x[0]) && is_int($x[1]) ||
                        count($x) == 3 && is_string($x[0]) && is_int($x[1]) && is_int($x[2]))
                    {
                        $order2[] = $x;
                    } else {
                        return false;
                    }
                } elseif (is_string($x)) {
                    $order2[] = array($x);
                } else {
                    return false;
                }
            }
        }
    }
    
    // array_multisortに渡すための一次元配列を作る
    $sort = array();
    foreach ($array as $a) {
        foreach ($order2 as $i => $o) {
            $sort[$i][] = $a[$o[0]];
        }
    }
    
    // array_multisortに渡す引数を組み立てて実行
    $params = array();
    foreach ($order2 as $i => &$o) {
        $params[] = &$sort[$i];
        if (count($o) > 1) {
            $params[] = &$o[1];
        }
        if (count($o) > 2) {
            $params[] = &$o[2];
        }
    } unset($o);
    $params[] = &$array;
    return call_user_func_array('array_multisort', $params);
}
