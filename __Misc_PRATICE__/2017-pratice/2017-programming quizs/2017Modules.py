# ##Get what modules installed
import pip
installed_packages = pip.get_installed_distributions()
installed_packages_list = sorted(["%s==%s" % (i.key, i.version)
     for i in installed_packages])
print(installed_packages_list)

# ###Installation: pip install numpy
# import numpy
# print( numpy.__version__ )##-->1.13.2

# # ###Installation: pip install plotly
# import plotly
# print( plotly.__version__ )##-->2.0.15

# # ###Installation: pip install mysqlclient
# import MySQLdb
# print( MySQLdb.__version__ )##-->1.3.12

# # ###Installation: pip install wxPython
# import wx
# print( wx.__version__ )##-->4.0.0b2