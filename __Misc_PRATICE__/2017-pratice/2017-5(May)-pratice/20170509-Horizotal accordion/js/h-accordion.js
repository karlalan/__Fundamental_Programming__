$(function(){
    //initial state
    $("dd:not(:first)").css("width","0px");
    $("dt:first span").addClass("selected");

    $("dl dt").click(function(){
        if($("+dd",this).css("width")=="0px"){
            $("dt:has(.selected)+dd").animate({"width":"0px"});
            $("+dd",this).animate({"width":"295px"});
            $("dt span.selected").removeClass("selected");
            $("span",this).addClass("selected");
        }
    }).mouseout(function(){
        $("span",this).removeClass("over");
    }).mouseover(function(){
        $("span",this).addClass("over");
    });
});