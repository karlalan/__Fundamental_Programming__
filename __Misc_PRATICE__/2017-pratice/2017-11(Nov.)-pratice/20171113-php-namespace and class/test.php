<?php
namespace T1{


  class Apple{


    protected $apple;

    function __construct(){
      global $guava;
      $this->apple = $guava;
    }


    public function callw(){
      return $this->apple;
    }

  }

}

namespace T2{
  use T1;
  $apple = new T1\Apple();
  global $a;
  $a = $apple->callw();

  class A{

    public $apple;
    public $re;

    function __construct(){
      global $king;
      global $a;
      $this->apple = $a;
      $this->re = $king;

    }

    public function callw(){
      echo $this->apple;
      echo $this->re;
    }

  }

}
