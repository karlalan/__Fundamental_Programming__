var CoinsNum = 10;

  window.onload = function(){
    // var CGY = new Catagory( CoinsNum );
    // CGY.MakeBlockCells();
    // setInterval( function() {
    //   CGY.getCoinsData();
    // }, 6000);

  }

  $( function() {
    $( "th" ).on( "click", function() {
        var BlockTableTh = $( this ),
            DoSort       = new SORT( BlockTableTh );
        DoSort.SortData();
    });
  });

  class Catagory{

    constructor( CoinsNum ){
      this.CoinsNum = CoinsNum;
    }
    /**
     * ブロックのタイトル
     */
    Blocks(){
      return [ "MarketCap", "Volume", "Price", "Change" ];
    }
    /**
     * テーブルのマスを作る
     */
    MakeBlockCells(){

      this.Blocks().forEach( function( Block ) {
          for( var i = 0; i < 10; ++i ){
            var tr       = document.createElement( "tr" ),
                img      = document.createElement( "img" ),
                aLink    = document.createElement( "a" ),
                spanImg  = document.createElement( "span" ),
                spanStr  = document.createElement( "span" ),
                tdRank   = document.createElement( "td" ),
                tdName   = document.createElement( "td" ),
                tdBlock  = document.createElement( "td" ),
                bNode = document.getElementById( Block + "_block" ),
                pNode = bNode.parentNode;
            //rank
            tdRank.id = i + "_" + Block;
            tr.appendChild( tdRank );
            //coin name and logo
            img.id  = i + "_logo_" + Block;
            spanImg.appendChild( img );
            tdName.appendChild( spanImg );
            aLink.id = i + "_link_" + Block;
            aLink.target = "_blank";
            spanStr.id = i + "_coin_" + Block;
            aLink.appendChild( spanStr );
            tdName.appendChild( aLink );
            tr.appendChild( tdName );
            //Block
            tdBlock.id = i + "_data_" + Block;
            tr.appendChild( tdBlock );
            //insert cells
            pNode.insertBefore( tr, null );
          }
      });

    }
    /**
     * ブロックによってそれぞれのデータを表示する
     * @param object BlocksData APIで取得してソートしてブロックに振り分けたデータ
     */
    MakeCoinsList( BlocksData ){

      var self = this;
      Object.keys( BlocksData ).forEach( function( Block ) {
          for( var i = 0; i < self.CoinsNum; ++i ){
            $( "#" + i + "_" + Block ).text( (i + 1) );
            $( "#" + i + "_" + Block ).closest( "tr" ).attr( "id", BlocksData[Block][i].coin );
            $( "#" + i + "_logo_" + Block ).attr( "src", Catagory.GetImg( BlocksData[Block][i].coin ) );
            $( "#" + i + "_link_" + Block ).attr( "href", Catagory.GetLink( BlocksData[Block][i].id ) );
            $( "#" + i + "_coin_" + Block ).html( "&nbsp;" + BlocksData[Block][i].coin );
            if( Block == "MarketCap" || Block == "Volume"  ){
              $( "#" + i + "_data_" + Block ).html( "$ " + Catagory.IntFormat( BlocksData[Block][i].data ) );
            } else if( Block == "Price" ){
              $( "#" + i + "_data_" + Block ).html( "$ " + Catagory.FloatFormat( BlocksData[Block][i].data , 4 ) );
            } else {
              ( BlocksData[Block][i].data < 0  ) ? $( "#" + i + "_data_" + Block ).css( "color", "red" ) : $( "#" + i + "_data_" + Block ).css( "color", "green" );
              $( "#" + i + "_data_" + Block ).text( BlocksData[Block][i].data + " %" );
            }
          }
      });

    }
    /**
     * 表示させたいデータをブロックによってまとめる。
     * @param object CoinsData APIから取得したデータ
     */
    MakeBlockData( CoinsData ){

      var BlocksData = {};
      this.Blocks().forEach( function( Block ) {
          BlocksData[Block] = [];
      });
      for( var i = 0; i < this.CoinsNum; ++i ){
        //MarketCap
        BlocksData["MarketCap"][i] = { "coin" : CoinsData[i].symbol,
                                       "id"   : CoinsData[i].id,
                                       "data" : Math.floor( CoinsData[i]["market_cap_usd"] ) };
        //Volume
        BlocksData["Volume"][i] = { "coin" : CoinsData[i].symbol,
                                    "id"   : CoinsData[i].id,
                                    "data" : Math.floor( CoinsData[i]["24h_volume_usd"] ) };
        //Price
        BlocksData["Price"][i] = { "coin" : CoinsData[i].symbol,
                                   "id"   : CoinsData[i].id,
                                   "data" : CoinsData[i]["price_usd"] };
        //Change
        BlocksData["Change"][i] = { "coin" : CoinsData[i].symbol,
                                    "id"   : CoinsData[i].id,
                                    "data" : CoinsData[i]["percent_change_24h"] };
      }
      SORT.RankSort( BlocksData["MarketCap"], "data", 1 );
      SORT.RankSort( BlocksData["Volume"], "data", 1 );
      SORT.RankSort( BlocksData["Price"], "data", 1 );
      SORT.RankSort( BlocksData["Change"], "data", 1 );
      this.MakeCoinsList( BlocksData );

    }
    /**
     * ajaxでphpへ要求してデータを取得する。
     * @return JSON APIから取得したデータ
     */
    getCoinsData(){
      var self = this;
      $.ajax({
          type: "GET",
          url:  "/api_coincap_catagory/",
          data: { "CoinsNum": self.CoinsNum },
          dataType: "json",
          success: function( json ){
            self.MakeBlockData( json );
          },
          error: function( XMLHttpRequest, textStatus, errorThrown ) {
              $( "#dataErr" ).text( "データエラー or APIエラー" );
          }
      });
    }

    /**
     * 数字のフォーマットやリンクやイメージを処理する様々な関数
     */
    static IntFormat( IntNumber ){ return new Intl.NumberFormat().format( IntNumber ); }
    static FloatFormat( FloatNum, n ){ return parseFloat( FloatNum ).toFixed( n ); }
    static GetLink( CoinId ){ return "https://coinmarketcap.com/currencies/" + CoinId; }
    static GetImg( CoinName ){ return "{$smarty.const.PATH_TOP_RELATIVE}dist/img/coinLogo/" + CoinName + ".png"; }

  }

  class SORT{

    constructor( DataTableTh ){
      this.DataTableTh = DataTableTh;
    }

    SortData(){

      //define variables
          var DataSortKey,
              DataOrderBy,
              DataTable = this.DataTableTh.closest( "table" ),
              DataTableID = DataTable.attr( "id" ),
              DataTableChildren = DataTable.children().children(),
              DataColNums,
              DataRowNums,
              DataTempArr = [],  //// get all entries and keep values in DataTempArray
              SortedData;

          // culumn no you select
          DataSortKey = this.DataTableTh.index();
          // toggle ASC or DESC
          ( this.DataTableTh.hasClass( "DESC" ) ) ? ( DataOrderBy = "ASC" ) : ( DataOrderBy = "DESC" );
          // initialize class
          DataTable.find( "th" ).removeClass( "ASC" );
          DataTable.find( "th" ).removeClass( "DESC" );
          this.DataTableTh.addClass( DataOrderBy );
          // get number of line and columns of the table
          DataRowNums = DataTableChildren.length;
          DataColNums = DataTableChildren.eq(0).children().length;

          for( var i = 1; i < DataRowNums; ++i ){
            DataTempArr[i-1] = [];
            for( var j = 0; j < DataColNums; ++j ){
              var value = DataTableChildren.eq(i).children().eq(j).text();
              if( j != 1 ){
                value = value.replace( "$ ", "" );
                value = value.replace( " %", "" );
                value = value.replace( /,/g, "" );
                DataTempArr[i-1][j] = Number( value );
              } else {
                DataTempArr[i-1][j] = value;
              }
            }
          }
          // sort by the key you selected
          ( DataOrderBy == "ASC" ) ? ( SortedData = SORT.RankSort( DataTempArr, DataSortKey, 1 ) ) : ( SortedData = SORT.RankSort( DataTempArr, DataSortKey, -1 ) );
          // insert DataTempArranged values into table
          for( var i = 0; i < SortedData.length; ++i ){
            for( var j = 0; j < SortedData[i].length; ++j ){
              switch( j ){
                case 2:
                  if( DataTableID == "Change" ){
                    DataTableChildren.eq(i+1).children().eq(j).text( SortedData[i][j] + " %" );
                    // ( SortedData[i][j] < 0  ) ? $( "#" + i + "_data_" + Block ).css( "color", "red" ) : $( "#" + i + "_data_" + Block ).css( "color", "green" );
                  }else{
                    if( DataTableID == "MarketCap" || DataTableID == "Volume" ){
                      DataTableChildren.eq(i+1).children().eq(j).text( "$ " + Catagory.IntFormat( SortedData[i][j] ) );
                    }else{
                      DataTableChildren.eq(i+1).children().eq(j).text( "$ " + Catagory.FloatFormat( SortedData[i][j] ) );
                    }
                  }
                  break;
                default:
                  DataTableChildren.eq(i+1).children().eq(j).text( SortedData[i][j] );
                  break;
              }

            }
          }

    }
    //sortkey = 1降順, -1昇順
    static RankSort( DataTempArr, DataSortKey, sortkey ){

      return DataTempArr.sort( function( a, b ) {
          if( Number( a[DataSortKey] ) != Number( b[DataSortKey] ) ){
            if( Number( a[DataSortKey] ) < Number( b[DataSortKey] ) ){
              return ( 1 * sortkey ) ;
            } else {
              return ( -1 * sortkey );
            }
          }
      });

    }


  }////class SORT end

</script>