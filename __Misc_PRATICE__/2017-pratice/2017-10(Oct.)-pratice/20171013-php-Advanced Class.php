<?php
###############Examplar############################
class X {
    private $non_static_member = 1;
    private static $static_member = 2;

    function __construct() {
        echo $this->non_static_member . ' '
           . self::$static_member;
    }
}

// new X();

// class X {
//     protected $non_static_member = 1;
//     protected $static_member = 2;

//     function __construct() {
//         echo $this->non_static_member . ' '
//            . self::$static_member;
//     }

//     protected function k(){
//       return $this->static_member*2;
//     }
// }


// class Y extends X {

//     function __construct() {
//         echo $this->k();
//     }
// }


// new Y();
#########################################################
// var_dump( hash( "md5", "apple" ) );
// var_dump( md5( "apple" ) );
// 


function DefendXSS( $data ){

    if( is_array( $data ) ){
      //for array data
      return array_map( "DefendXSS", $data );
    } else {
      return htmlspecialchars( $data, ENT_QUOTES );
    }
}