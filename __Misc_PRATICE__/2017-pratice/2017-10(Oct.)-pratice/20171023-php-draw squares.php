<?php
// 200 x 200 の画像を作成します
$canvas = imagecreatetruecolor(450, 450);

// 色を割り当てます
$pink = imagecolorallocate($canvas, 255, 105, 180);
$white = imagecolorallocate($canvas, 255, 255, 255);
$green = imagecolorallocate($canvas, 132, 135, 28);

// 3 つの矩形をそれぞれの色で描画します
imagerectangle($canvas, 100, 100, 150, 150, $pink);
imagerectangle($canvas, 45, 60, 120, 100, $white);
imagerectangle($canvas, 100, 120, 75, 160, $green);

// for( $i = 0; $i < 100; $i+50 ){
//   for( $j = 0; $j < 500; $j+50 ){
//     imagerectangle($canvas,$i, $i, $j, $j, $white);
//   }
// }

// 出力してメモリから解放します
header('Content-Type: image/jpeg');

imagejpeg($canvas);
imagedestroy($canvas);