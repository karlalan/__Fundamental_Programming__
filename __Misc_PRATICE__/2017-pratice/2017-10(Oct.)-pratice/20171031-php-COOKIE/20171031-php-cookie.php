<?php
/**
 * Current domain: localhost
 */
// var_dump( $_SERVER['SERVER_NAME'] );

/********************************************************************
 * setcookie(name,value,expire,path,domain,secure,httponly)
 *
 * time() is current time.
 *
 * Ref: https://www.w3schools.com/php/func_http_setcookie.asp
 *      http://php-beginner.com/practice/cookie/cookie5.html
 ********************************************************************/

/**
 * setting for expiration of cookie
 *
 * The value: time()+86400*30, will set the cookie to expire in 30 days.
 * If this parameter is omitted or set to 0, the cookie will expire at the end of the session (when the
 * browser closes). Default is 0
 *
 *
 * @var [type]
 */
$n = 0.0001;
$days_for_cookie = ( 86400 * ($n) );//----->$n is n days from now
$expire = time() + $days_for_cookie;

/**
 * setting for path
 *
 * If set to "/", the cookie will be available within the entire domain.
 * If set to "/php/", the cookie will only be available within the php directory and all sub-directories of php.
 *The default value is the current directory that the cookie is being set in
 *
 * @var string
 */
$path = "/";

/**
 * Specifies the domain name of the cookie. To make the cookie available on all subdomains of example.com,
 * set domain to "example.com". Setting it to www.example.com will make the cookie only available in the
 * www subdomain
 *
 * @var global scope variable
 */
$domain = $_SERVER['SERVER_NAME'];


/**
 * Specifies whether or not the cookie should only be transmitted over a secure HTTPS connection.
 * TRUE indicates that the cookie will only be set if a secure connection exists. Default is FALSE
 *
 *
 * @var [boolean]
 */
$secure = FALSE;


/**
 * If set to TRUE the cookie will be accessible only through the HTTP protocol (the cookie will not be
 * accessible by scripting languages). This setting can help to reduce identity theft through XSS attacks.
 * Default is FALSE
 *
 * @var [boolean]
 */
$httponly = TRUE;

$cookiename  = "Is_this_cookie";
$cookievalue = "javascript also can be written";

setcookie( $cookiename, $cookievalue, $expire, $path, $domain, $secure, $httponly );

/**
 * Note
 *      The output of $_COOKIE is an array that storages the set cookie name and values.
 */

?>