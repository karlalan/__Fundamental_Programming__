<?php
$arr1 = ["a","b","c","d","e"];
$arr2 = ["1","2","3","4","5","6"];

//************This can be applied to any array without recode******************

function combine(array $array1, array $array2){
//*********count the length of arrays******************************************
    $array1_len = count($array1);
    $array2_len = count($array2);
    $array = [];
//***********************************for first situation************************
    if ($array1_len >= $array2_len) {
        for ($i = 1; $i <= ($array1_len + $array2_len); $i++) {
            switch($i) {
                case $i % 2 == 0 && $i <= (2 * $array2_len):
                    $array[$i] = $array2[$i/2 - 1];
                    break;
                case $i % 2 != 0 && $i <= (2 * $array2_len):
                    $array[$i] = $array1[($i + 1)/2 - 1];
                    break;
                case $i > (2 * $array2_len):
                    $array[$i] = $array1[$i - $array2_len - 1];
                    break;
            }
        } print_r ($array);
//********************************for second situation************************
    } else {
        for ($i = 1; $i <= ($array1_len + $array2_len); $i++) {
            switch($i) {
                case $i % 2 == 0 && $i <= (2 * $array1_len):
                    $array[$i] = $array2[$i/2 - 1];
                    break;
                case $i % 2 != 0 && $i <= (2 * $array1_len):
                    $array[$i] = $array1[($i + 1)/2 - 1];
                    break;
                case $i > (2 * $array1_len):
                    $array[$i] = $array2[$i - $array1_len -1];
                    break;
            }
        } print_r ($array);
    }
}
//*************execute function************************************************
combine($arr1,$arr2);


//**********************this cannot be applied to any array**********************
// $array = [];
// for ($i = 1; $i <= 6; $i++) {
//     switch($i) {
//         case $i % 2 == 0:
//             $array[$i] = $arr2[$i/2 - 1];
//         break;
//         case $i % 2 != 0:
//             $array[$i] = $arr1[($i + 1)/2 - 1];
//         break;
//     }
// }
// var_dump($array);