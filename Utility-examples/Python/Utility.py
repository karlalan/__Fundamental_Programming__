class Util():
  ## @staticmethod
  ## if u want to make the method be static when the Util object is instantiated
  def IsList( target ):
    return type(target) is list

  def IsString( target ):
    return type(target) is str

  def IsSet( target ):
    return type(target) is set

  def IsDictionary( target ):
    return type(target) is dict

  def IsInteger( target ):
    return type(target) is int

  def IsFloat( target ):
    return type(target) is float

  def IsComplex( target ):
    return type(target) is complex

  def IsNumber( target ):
    return Util.IsInteger(target) | Util.IsFloat(target)
  